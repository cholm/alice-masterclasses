Contributing
============

A new class
-----------

Fork this project and make your changes there.  Once your done, make a pull request. 

- New master classes can be added following the templates given in the
  `Raa` and `Strangeness` sub-directories.  
  
- The top-level GUI will automatically detect the presence of master
  classes if the script `Class.C` is present in the sub-directory.
  That script is supposed to return a `TList` with key-value pairs
  (`TNamed` objects) in a tree like 
  
	  . -+- name (Name of master class)
	     |
		 +- desc (File path to HTML description)
		 |
		 +- exer (A `TList` of exercises)
		    |
			+ name of exercise (title is script to execute)
			|
			+ ...
			
  The `TNamed` objects in the `exer` list will give the name of the
  exercise, and (as the title) the script to execute relative to the
  top directory).  Note, the script _must_ accept a single boolean
  parameter which selects _cheat_ mode if available.
  
- Should the master class need the event display, it should define a
  class that derives from `AliceEventDisplay.` 
  
- Other exercises should be specified via sub-classes of
  `AliceExercise`.
  
Translations
------------

The code is set-up to provide translated descriptions of the master classes.  
However, these translations needs to be done, so please contribute if you can. 

- Find the various HTML files in the project and make translations of the the text.
  You can get a comprehensive list by running. 
  
    find . -name "*.html"
    
    
- Translate a file to your favorite language.  
- Then save it with the language code before the final `.html` - e.g., 
  `Description.fr.html` for French, 'Description.de.html' for German, and so on. 
- Submit the translated file in an issue against this project. 

 