FROM gitlab-registry.cern.ch/cholm/alice-masterclasses:nodata
LABEL maintainer="christian.holm.christensen@cern.ch"

# --- Install needed packages ----------------------------------------
USER root 
RUN yum install -y \
    	liberation-sans-fonts \
    	net-tools \	
    	openbox \
    	python-websockify \
	python2-pip \
	supervisor \
	sudo \
	tint2 \
	which \
	x11vnc  \
	xorg-x11-drv-dummy  \
	xorg-x11-drv-void \
	xorg-x11-xinit \
	xorg-x11-xinit-session \
        xterm && \
	rm -f /usr/share/applications/x11vnc.desktop && \
	pip install supervisor-stdout && \
	yum clean -y all

# --- Get and install noVNC ------------------------------------------
# https://github.com/thewtex/docker-opengl uses the hash
# 6a90803feb124791960e3962e328aa3cfb729aeb - we'll try the master
RUN git clone https://github.com/kanaka/noVNC.git /opt/noVNC && \
    cd /opt/noVNC                                            && \
    git checkout v0.6.2                                      && \
    ln -s vnc_auto.html index.html

# --- Copy stuff to file-system --------------------------------------
COPY .docker/etc /etc
RUN cp -a /etc/skel/.xinitrc /etc/skel/.local /home/student/ && \
    chown -R student:exercise /home/student/.xinitrc /home/student/.local

# --- Set-up student directory ---------------------------------------
WORKDIR	    /home/student

# --- Exposed ports --------------------------------------------------
EXPOSE 6080 5900

# --- Other settings in the image ------------------------------------
ENV   	    DISPLAY :0

# --- Entry point ----------------------------------------------------
ENTRYPOINT ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]

# --- App to execute -------------------------------------------------
ENV APP /usr/bin/xterm 
ENV ARGS -ls -sb -e /opt/root/bin/root -l /opt/AliceMasterClass/MasterClass.C

#
# EOF
#
