Docker image of the ALICE Master Classes
========================================

The provided [Docker][docker] image contains all you need to run the
ALICE Master Classes on a Docker enabled host system. 

The images comes in two variants: 

- Images that uses the X Window System (X11) to display the
  interfaces.
- Images that uses a web-browser to display the interfaces. 

Each of these two variants comes in two additional flavors: _with_ and
_without_ data files.  If storage and download time is not an issue
for you, you should take the flavors that _contain_ the data.  If you
are deploying these master classes over a wide number of hosts, you
may want to download and host the data files in one place.  You can
put them on any web-server accessible to your clients.

The images are 

- Using X11 
  - `gitlab-registry.cern.ch/cholm/alice-masterclasses`
	Contains the data
  - `gitlab-registry.cern.ch/cholm/alice-masterclasses:nodata`
	_Does not_ contain the data
- Using Web-browser
  - `gitlab-registry.cern.ch/cholm/alice-masterclasses:web`
	Contains the data
  - `gitlab-registry.cern.ch/cholm/alice-masterclasses:webnodata`
	_Does not_ contain the data

Prerequisites 
-------------

You should have about 2.5GB of storage available if you run the images
that _does_ contain the data.  If you run the images _without_ data
you need about 1.5GB. 

You should be running GNU/Linux, MacOSX, or Windows. 

You need some software to be able run the image.

- First and foremost you need Docker.
- If you want to use X11 to display the interface, you also need an X
  server that supports hardware acceleration via OpenGL (see also
  [more below](#opengl)). If you have trouble getting hardware
  acceleration to work through the container you can use the
  web-browser images instead. 
- If you want to use a web-browser to display the interface, you need
  a fairly modern web-browser. Recent versions of Chrome, Firefox,
  Safari, and Edge should all do.
  
### GNU/Linux

- _Docker_: you can use the distribution tools to get the package
  e.g.,
	
		sudo apt-get install docker.io # Debian based
		sudo yum install docker        # Red Hat based

- _X11_: You need to allow Docker to access the display, so you must
   run
	  
		xhost +root:local
	  
### MacOSX
- _Docker_: You  must get [Docker for Mac][dockermac]. 

- _X11_: Get [XQuartz][xquartz].  You need to adjust the settings of
  the X server so that Docker may access the display.
	  
  - Start XQuartz e.g., in a terminal write 
	  
		open -a XQuartz 
		
  - In the menu of Quartz, tick

		Settings -> Security -> Allow connections from network clients

  - In an X-terminal (started from the XQuartz tool-bar), you must
	allow docker to access the display

		xhost +root:local

### Windows

- _Docker_: You must get [Docker for Windows][dockerwin]. 

- _X11_: Get [Xming][xming].  To allow Docker to access the display,
  consult the documentation of Xming to allow remote access to the
  display. 
	  
Installation
------------

You do not have to download the images before execution, but to speed
up the execution, you may want to.  To get the image on to the local
host, simply run 

	docker pull gitlab-registry.cern.ch/cholm/alice-masterclasses 
	
or one of the variants listed above. 
	
Usage
-----

Executing the Docker image will automatically start the master class
graphical user interface (see also
[usage instructions](../README.md#usage))

Optionally, you can download [this script](run.sh) to ease the
execution.  Just do 

	./run.sh --help 
	
to get a list of options. 

### Using X11 

First, we need to make sure the X11 display is running.

- *GNU/Linux*: X11 is running if you have a desktop environment. If
  you don't, consult the distribution documentation for instructions
  on how to set-up a desktop environment.
- *MacOSX*: Start up XQuartz, either from the Launcher, Finder, or via
  the command

		open -a XQuartz
		
  Then fire up an X-terminal from the XQuartz menu bar. 
		
- *Windows*: Start up Xming. Do this from the application menu.  Next,
  fire up an X-terminal if you haven't already. 
  
Next, we need to execute the Docker image 

		docker run \
			--interactive \
			--env=DISPLAY \
			--volume=/tmp/.X11-unix:/tmp/.X11-unix \
			gitlab-registry.cern.ch/cholm/alice-masterclasses 

and you should see the GUI to select the master class.

### Using a web-browser 

Execute the docker image 

	docker run -p 6080:6080 \
		gitlab-registry.cern.ch/cholm/alice-masterclasses:web

and then direct your web-browser to 

	http://localhost:6080 

and you should see the GUI to select the master class.

### Using external data 

If you choose to take one of the images without the data, you may want
to set-up some local storage where the data can be found.  You have a
couple of options for this 

- Store the data on a local web-server.  For example, you may be
  running a web-server with the address `http://some.domain/`.  You
  can then upload the data files to that web server - say at the URL
  `http://some.domain/alice-masterclasses-data`. You should then ask
  the students to use that URL when selecting the data source. 
  
  - If you use [the supplied script](run.sh) you can pass the options 
  
		-n -d http://some.domain/alice-masterclasses-data
		
	and this will be done automatically
  
- Use a docker volume.  You can prepare a volume (either a Docker
  managed volume or some NFS shared directory) and then pass that
  volume at run time e.g., 
  
		docker run -p 6080:6080 -v SOURCE:/home/student/data \
			gitlab-registry.cern.ch/cholm/alice-masterclasses:web

  where _SOURCE_ is your volume (either Docker managed or some NFS
  shared directory).  You should then ask the students to input `data`
  when selecting the data source. 
  
  - The data files _must_ be in the directory hierarchy 
  
		-+- Base 
		 |   `- data
		 +- Raa
		 |   +- data
		 |   `- vsdData
		 `- Strangeness
		     +- data
			 `- vsdData 
			 
  - If you use [the supplied script](run.sh) you can pass the options 
  
		-n -d SOURCE 
		
	and this will be done automatically. 
	
  - If you use a docker volume or volume container, make sure that it
    exports the volume `/home/student/data`.  To set-up the volume,
    get the
    [full distribution](https://gitlab.cern.ch/cholm/alice-masterclasses/builds/artifacts/master/download?job=fulldist)
    and unpack it somewhere (perhaps and NFS drive)
	
		docker volume create alice-masterclasses-data
		cd AlICE_MasterClass-master
		tar c */data */vsdData | \ 
		  docker run --interactive --rm \
		    -v alice-masterclasses-data:/home/student/data \
			busybox tar x -C /home/student/data
			
    You can now specify `alice-masterclasses-data` as the _SOURCE_ above

  - For deployment over _many_ machines, it may not be so useful to
    download the data on all hosts.  In that case, you can set-up an
    NFS server (or some other shared file system) accessible to the
    hosts.  Suppose you've put the data files on the host `master.domain` in
    the directory `/data/alice-masterclasses/`, which you export with
    a line like 
	
		/data/alice-masterclasses node*.domain(ro,no_subtree_check)
		
	in `/etc/exports` and you mount this on the hosts `node*.domain`
    with 
	
	    master.domain:/data/alice-masterclasses /data/alice-masterclasses nfs defaults 0 0

	in the `node*.domain`s `/etc/fstab`.  Then you can specify
    `/data/alice-masterclasses` as _SOURCE_ in the above.  Note that
    you _must_ have above mentioned directory structure. 
	
### Persistent storage of results

The container (and all it's generated data) ceases to exist after the
session ends.  However, in some exercises, the students are asked to
store some results.  To facilitate that, one should add a _volume_
that maps to the host system.  More on this later.

In the mean time, you can copy the results out as 

	docker cp CONTAINER:/home/student/FILE FILE 
	
where _CONTAINER_ is the name of the container (do `docker ps`), and
_FILE_ is the name of the file of interest.

OpenGL
------

The master classes uses OpenGL hardware acceleration to draw 3D images
of the ALICE detectors and events.  For this to work, the Docker image
needs to be able to use the hosts Direct Rendering Interface
(DRI). How well that works depends on the host system 

### GNU/Linux 

Most graphics cards and their drivers works seamlessly with Docker
because they implement proper abstraction layers in drivers and
interface libraries that work through the X11 standard protocol.
However, some hardware manufactures requires rather specialized access
to hardware through proprietary drivers and interface libraries (most
notably nVidia - see [what Linus Thorvalds had to say about
that][ltnvidia]), which makes it next to impossible to have the Docker
(or any other indirect means) work with these graphics cards.

In short, your millage with the Docker image and hardware acceleration
may depend on the kind of support the hardware manufacture of your
graphics card provides on GNU/Linux.  If the Docker image does not
work for you, you can always use the direct installation on the host
machine (see [these instructions](README))
  
Links
-----
[docker]: http://docker.com
[dockermac]: https://docs.docker.com/docker-for-mac/
[xquartz]: https://www.xquartz.org/
[dockerwin]: https://docs.docker.com/docker-for-windows/
[xming]: https://sourceforge.net/projects/xming/
[rootdocker]: https://hub.docker.com/r/rootproject/root-ubuntu16/
[ltnvidia]: https://www.youtube.com/watch?v=IVpOyKCNZYw
