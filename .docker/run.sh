#!/bin/bash

# --- Some colors ----------------------------------------------------
c_red='\033[0;31m'
c_green='\033[0;32m'
c_orange='\033[0;33m'
c_blue='\033[0;34m'
c_purple='\033[0;35m'
c_cyan='\033[0;36m'
c_clr='\033[0m'
# --- Some default values --------------------------------------------
container=
base_image=gitlab-registry.cern.ch/cholm/alice-masterclasses
image_kind=web
image_var=
web_opt="--publish=6080:6080"
xdisplay_opt="--env=DISPLAY"
xsocket_opt="--volume=/tmp/.X11-unix:/tmp/.X11-unix"
xmore_opt="--interactive --tty=true"
data_log=

# --- Show usage -----------------------------------------------------
usage()
{
    cat<<-EOF
	Usage: $0 [OPTIONS]

	-h,--help		This help 
	-k,--kind     KIND	Which kind of interface to use
	-n,--no-data		Run images without data
	-d,--data     LOCATION	Location of data (if using data-free images)	

	KIND can be one of

	- web   Use a webbrowser for interfaces 
	- x11   Use native X Window System interfaces 

	If no KIND is specified, then "${image_kind}" is assumed. 

	Note, for the "x11" kind your HW/OS/docker stack must support
	direct rendering using OpenGL.  Some graphics cards/drivers
	(most notably nVidia) are not supported.  In that case, use
	the "web" kind. 

	If you choose to run an image without the data preloaded (by
	passing the option -n/--no-data) you can specify the location
	of the data by passing the option -d/--data.  LOCATION can
	then be a directory accessible on the host system, the name of
	a data volume or data volume container, or a URL to a
	web-service that hosts the data.

	EOF
}

# --- Print error message --------------------------------------------
err()
{
    echo -e "${c_red}Error${c_clr}: ${c_orange}" $@ "${c_clr}"> /dev/stderr
    exit 1
}

# --- Check for installation of Docker -------------------------------
check_docker()
{
    if which docker > /dev/null 2>&1 ; then return 0 ; fi
    cat <<-EOF
	Error: Docker is not installed/found 
	
	Check your installation.  If you have not installed Docker, you should.

	GNU/Linux: sudo apt-get install docker.io     
		   sudo yum install docker 

	MacOS/X:   https://docs.docker.com/docker-for-mac
	Windows:   https://docs.docker.com/docker-for-windows

	EOF
    exit 1
}

# --- Function to clean-up running instances -------------------------
cleanup()
{
    running=$(docker ps -a -q --filter name=$container)
    if test "X$running" != "X" ; then
	docker top $container
	docker rm $container
    fi
}

# --- If not on Linux, create a virtual machine ----------------------
spawn_machine()
{
    if test $(uname) = "Linux" ; then return 0 ; fi
    # Check for active machines, and spawn one if not found 
    vm=$(docker-machine active 2>/dev/null || echo "default")
    if ! docker-machine inspect "${vm}" &>/dev/null ; then
	docker-machine -D create -d virtual-box --virtual-memory 2048 ${wm}
    fi
    docker-machine start ${vm} >/dev/null
    eval $(docker-machine env $vm --shell=sh)
    ip=$(docker-machine ip ${vm} 2>/dev/null || echo "localhost")
}

# --- Check the display ----------------------------------------------
check_display()
{
    if test $(uname) = "Linux" ; then
	if test "X$DISPLAY" = "X" ; then
	    err "DISPLAY environment variable not set - is X running?"
	fi 
	return 0
    elif test $(uname) = "Darwin" ; then
	if test "X$DISPLAY" = "X" ; then
	    # Try to start XQuartz
	    open -a XQuartz
	fi
	if test "X$DISPLAY" = "X" ; then
	    err "XQuartz not running/installed"
	fi
	# Get host IP - first cabled 
	ip=$(ipconfig getifaddr en0)
	if test "X$ip" = "X" ; then
	    ip=$(ipconfig getifaddr en1)
	fi
	if test "x$ip" = "x" ; then
	    err "Failed to get IP address of host"
	fi
	xdisplay_opt="--env=DISPLAY=${ip}:${DISPLAY}"
    fi
}

# --- Process command line arguments ---------------------------------
while test $# -gt 0 ; do
    case $1 in
	-h|--help)		usage; exit 0 ;;
	-k|--kind)	shift; image_kind=`echo $1 | tr [A-Z] [a-z]`;;
	-n|--no-data)	image_var=nodata ;;
	-d|--data)	shift; data_loc=$1 ;;
	--) shift ; break ;;
	*)  err "Unknown argument: $1" ;;
    esac
    shift
done

# --- Figure out arguments to run ------------------------------------
image_tag=
container="alice-masterclasses-${image_kind}${imagevar}"
case x$image_kind in
    xweb)
	image_tag=web
	image_opt="$web_opt"
	image_end="&"
	;;
    xx11)
	check_display
	image_tag=
	image_opt="$xdisplay_opt $xsocket_opt $xmore_opt"
	;;
    x*) err "Unknown kind: $image_kind" ;;
esac
image_opt="--name ${container} --rm ${image_opt}"
image_tag="${image_tag}${image_var}"
image=$base_image
if test "x$image_tag" != "x" ; then image="${image}:${image_tag}" ; fi

# Check to see if we run the nodata image and if we have an image
# location.
if test "X$image_var" = "Xnodata" && test "X$data_loc" != "X" ; then
    # Check if this is a directory
    data_vol_opt=
    data_tgt_dir=/home/student/data
    data_env_opt="--env=ALICE_MASTERCLASS_DATA=${data_tgt_dir}"
    vol_test=$(docker volume list -f "name=$data_loc" -q)
    con_test=$(docker ps -f "name=$data_loc" -q)
    if test -d $data_loc ; then
	data_loc=$(readlink -f ${data_loc})
	# Check the content of the directory
	if test ! -d $data_loc/Base/data || \
		test ! -d $data_loc/Raa/data || \
		test ! -d $data_loc/Strangeness/data ; then
	    err "Specified data directory does not contain the data"
	fi
	# Bind-mount the directory in container
	data_vol_opt="--volume=${data_loc}:${data_tgt_dir}"
    elif test "x$vol_test" != "x" ; then 
	# we have a volume with the given name - mount that
	data_vol_opt="--volume=${data_loc}:${data_tgt_dir}"
    elif test "x$con_test" != "x"  ; then
	# We have a container with that name.  Check that it has the
	# right volume.
	if ! docker inspect -f '{{range .Mounts}}{{.Destination}} {{end}}' \
	     ${data_loc} | grep ${data_tgt_dir} >/dev/null 2>&1 ; then
	    err "Container ${data_loc} does not have the volume ${data_tgt_dir}"
	fi
	data_vol_opt="--volumes-from ${data_loc}"
    elif [[ $data_loc =~ ^http:// ]] || [[ $data_loc =~ ^https:// ]] || \
	     [[ $data_loc =~ root:// ]] || [[ $data_loc =~ rootd:// ]] ; then
	# We found a remote hosting data location
	data_env_opt="--env=ALICE_MASTERCLASS_DATA=${data_loc}"
    else
	# If we get here, we did not find the local directory, nor did
	# we find a docker volume or volume container with the
	# specified name, or a recognised remote hosting service
	err "Didn't find directory, volume, or remote service for data"
    fi
    image_opt="$image_opt ${data_vol_opt} ${data_env_opt}"
fi

echo -e "${c_green}Will run${c_clr}"
echo -e ""
echo -e "docker run ${c_cyan}${image_opt} ${c_orange}${image}${c_clr}"


# --- In case we're using the web-browser, wait for it	
if [[ $image_tag =~ web ]]; then
    docker run ${image_opt} $@ ${image} &
    
    url=http://localhost:6080
    sleep 3
    if which xdg-open > /dev/null 2>&1 ; then
	xdg-open $url
    elif which open > /dev/null 2>&1 ; then
	open $url
    else
	echo -e ""
	echo -e "${c_green}Point your browser at ${c_orange}${url}${c_clr}"
	echo -e ""
    fi
    
    docker wait $container >/dev/null
else
    exec docker run ${image_opt} $@ ${image}
fi 


#
# EOF
#


	      


