\documentclass[11pt,twoside]{article}

\usepackage[margin=2cm,a4paper]{geometry}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amstext}
\usepackage{listings}
\usepackage{hyperref}
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}
\DeclareRobustCommand\RAA{\ensuremath R_{\mathrm{AA}}}
\DeclareRobustCommand\RCP{\ensuremath R_{\mathrm{CP}}}
\DeclareRobustCommand\YAA{\ensuremath Y(\mathrm{Pb-Pb})}
\DeclareRobustCommand\Ypp{\ensuremath Y(\mathrm{pp})}
\DeclareRobustCommand\Ncoll{\ensuremath%
  \left\langle N_{\mathrm{coll}}\right\rangle}
\DeclareRobustCommand\pT{\ensuremath p_{\scriptscriptstyle\mathrm{T}}}
\DeclareRobustCommand\sNN[1]{\ensuremath%
  \sqrt{s_{\scriptscriptstyle\mathrm{NN}}}=#1\,\mathrm{TeV}}
\title{Measurement of the nuclear modification factor $\RAA{}$ with
  ALICE}%
\author{ALICE Collaboration}%
\date{\today}
\setlength{\parindent}{0pt}
\setlength{\parskip}{1ex}

\begin{document}
\thispagestyle{empty}
\newdimen\tempdima
\tempdima=\textwidth
\advance\tempdima-4.5cm
{\makeatletter\xdef\ddate{\@date}}
\begin{tabular}[t]{@{}p{2cm}@{}p{\tempdima}@{}p{2cm}@{}}
  \multicolumn{3}{c}{\Large EUROPEAN ORGANIZATION FOR NUCLEAR RESEARCH}\\[8mm]
  \includegraphics[height=2cm]{../../Base/doc//ALICE_logo}
  &\hfill
  &\includegraphics[height=2cm]{../../Base/doc/CERN_logo}\\[3mm]
  &
  \multicolumn{2}{r}{\ddate}
\end{tabular}
\begin{center}
  {\makeatletter\Large\textbf{\@title}}
  \par
  \vspace*{5mm}
  \begin{tabular}[t]{c}
    {\makeatletter\large\@author}
  \end{tabular}
\end{center}
{\let\thempfn\empty
  \footnotetext{\hspace*{-1.5\parindent}\noindent\normalsize%
    \copyright\,\the\year\ CERN for the benefit of the ALICE
    Collaboration.\newline%
    Reproduction of this article or parts of it is allowed as specified
    in the \href{http://creativecommons.org/licenses/by/4.0}{CC-BY-4.0
      license}.}}
\tableofcontents

\section{Physics motivation}

\subsection{The ALICE Detector System}
ALICE (A Large Ion Collider Experiment) is one of the four large
experiments at the CERN Large Hadron Collider. ALICE has been designed
to study heavy-ion collisions. It also investigates proton-proton (pp)
collisions, which primarily provide reference data for the heavy-ion
program but, in addition, allow for a number of genuine proton-proton
physics studies.

ALICE was designed to cope with the highest particle multiplicities
anticipated for collisions of lead nuclei at the extreme energies of
the LHC. It is composed out of different subdetectors. In the Central
Barrel these are the Inner Tracking System (ITS) consisting of three
high resolution silicon detectors systems featuring two layers each,
the Time Projection Chamber (TPC), the Transition Radiation Detector
(TRD), the Time Of Flight detector (TOF), the High Momentum Particle
Identification Detector (HMPID), two electromagnetic calorimeters
EMCal and PHOS, and ACCORDE, a dedicated cosmic ray detector. In the
following analysis mainly the TPC and to some extent the ITS will be
used.

\subsection{Nuclear Modification Factor $R_{\mathrm{AA}}$}

In the year 2010, data from the first heavy-ion collisions at the LHC
were recorded by the experiments. ALICE, which is well suited for the
measurement of the properties of particles in high particle density
environments, has measured the nuclear modification factor $\RAA{}$
for unidentified charged particles. $\RAA{}$ is a measure for the
difference in particle production in proton--proton and
nucleus--nucleus collisions, taking into account the different
collision geometries. $\RAA{}$ is defined as:
\begin{align}
  \label{eq:raa}
  \RAA = \frac{\YAA}{\Ncoll\,\Ypp}
\end{align}
where $\YAA$ and $\Ypp$ are the yield (or number of particles per
event) in Pb--Pb and pp collisions, respectively. $\Ncoll$ is the
average number of so-called binary nucleon--nucleon collisions in the
collision of two heavy-ions (e.g., Pb-Pb). Since each lead nucleus
consists of 208 nucleons i.e., 82 protons and 126 neutrons, one could
think of a typical Pb--Pb collisions as a superposition of many
individual nucleon-nucleon collisions, and $\Ncoll$, which cannot be
determined in the experiment, expresses how many of such pp collisions
a given Pb--Pb collision would correspond to. 

If the nuclear modification factor is equal to one, the production of
particles in one Pb--Pb collision is on average the same as in
$\Ncoll$ independent pp collisions. Naively, this could be interpreted
such that the physics of pp and Pb--Pb collisions is the same.

\subsection{Centrality}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.9\linewidth]{Overlap}
  \caption{Illustrations of the two colliding nuclei, indicating the
    impact parameter $b$ (left), the participating nucleons, as well as
    the spectators (right).}
  \label{fig:impact}
\end{figure}

The large size of lead ion, compared to a proton, leads to an
observable difference between Pb--Pb and pp collisions. In pp
collisions there is only 1 collision (not taking into account
ultraperipheral collisions), while in the collision of Pb--Pb we need
differentiate between the different sizes of the overlap region. If
the overlap region is the largest, then highest energy densities can
be reached and a new state of matter, the quark gluon plasma, will
possibly be reached. As the overlap region of the two ions gets
smaller, the energy density reduces as well.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.5\linewidth]{Centrality}
  \caption{Top: Correlation between V0 amplitude and the Time
    Projection Chamber (TPC) track multiplicity measured by the ALICE
    collaboration. Bottom: Minimum bias distribution of the TPC track
    multiplicity (black line), as well as the TPC track distribution
    in the $70-80\%$, $30-40\%$, and $0-5\%$ centrality classes (gray)
    for Pb--Pb collisions at $\sNN{2.76}$.}
  \label{fig:centrality}
\end{figure}

To see the difference between pp and Pb--Pb collisions one first has
to define event classes for heavy-ion collisions. One of the criteria
to define such classes is the collision centrality: an event selection
related to the impact parameter $b$ (the distance between the
colliding nuclei perpendicular to the beam axis, see also
Fig.~\ref{fig:impact}). However this  parameter is not directly
measurable, but it can be determined via multiplicity measurements and
model fits to these distributions. In the most central events
($0-5\%$) the two ions collide head--on. Peripheral events ($>70\%$
centrality), on the other hand, should similar to pp collisions, since
we have on average fewer collisions between the nucleons. Quantitative
estimates of the collision centrality  are given by the number of
participating nucleons $N_{\mathrm{part}}$, binary nucleon-nucleon
collisions
$N_{\mathrm{coll}}$ or spectators $N_{\mathrm{spec}}$
$$
N_{\mathrm{spec}} = 2A - N_{\mathrm{part}}\quad,
$$
where $A$ is the mass number of the initial nuclei (e.g, 208 for Pb),
or by the forward hadronic energy $E_{\mathrm{ZDC}}$.  These
quantities can be related to the impact parameter via Glauber Model
calculations. ALICE can measure the centrality in four
different ways: via the energy deposition in the ZDC or via the
multiplicity of particles measured in the SPD, VZERO or TPC
detectors. The correlation of two of these measurements is shown in
upper part of Fig.~\ref{fig:centrality}.  In the lower part of the
same figure the distributions of the uncorrected the number of TPC
tracks is shown for all events, as well as for a few centrality
classes.

The determination of $\RAA{}$ as function of the centrality gives
insight to the properties of the medium, since the production of
particles in a medium is compared to the production of particles
without a medium.

\subsection{$\RAA{}$ as function of the transverse momentum}

For all ecentrality classes the nuclear modification factor $\RAA$ can
be measured as a function of the transverse momentum $\pT$ of charged
particles. $\pT$ is the momentum component of the particle in the 
xy-plane perpendicular to the beam axis z. It is given by
$$
\pT = \sqrt{p_x^2+p_y^2}
$$

It is an interesting question whether $\RAA$ depends on the transverse
momentum of a particle. Charged particle with large $\pT$ typically
originate from violent scattering processes of quarks or gluons in the
incoming nuclei (called hard processes). These quarks or gluons
propagate through the hot and dense medium produced in the collision
and they interact with this medium. This interaction, in general, will
lead to energy loss of the fast moving quark or gluon and that could
leave its footprint on $\RAA{}$.

Clearly, one would expect that for the most peripheral collisions
i.e., the centrality class $70-80\%$ in the current case, $\RAA$ is
close to unity (1) because a peripheral Pb--Pb collision probably
similar to pp collisions, at least in terms of particle production. To
find out what happens in more central collisions is the purpose of  
the current analysis.

\section{Visual Analysis}
\subsection{The Task}

The goal of this visual analysis is to make you familar with the
concepts of:

\begin{description}
\item[Clusters:] electronic signature left by a particle traversing a 
  detector, with additional information about time or space
\item[Track:]
  Path of a praticle trough the detectorssystem, reconstructed on the
  basis of the space and time information of individual clusters. The
  path of the particle is straight in an environment without
  additional forces i.e., no magnetic (B) or electric fields (E). If
  a charged particle is traversing a magnetic field it will be
  influenced by the magnetic field due to the Lorentz force and the
  tracks will be curved.
\item[Interaction point:] collision vertex selected for the analysis 
\item[Sattelite collisions/pile-up vertices:] collisions
  vertices seen, but not selected, in the detector due to high  
  intensity of the LHC-beam
\item[Primary track:] track originating from the selected collision
  interaction point (for this analysis: distance of closest approach
  $<1\,\mathrm{cm}$)
\item[Secondary track:] track not originating from the interaction
  point but from a secondary decay vertex (for example strange
  particle decays)
\end{description}

Your task will be to count the number of tracks originating from the
primary vertex (multiplicity) by clicking on each primary track.  You
will do this for 30 pp collisions at a centre of mass energy of
$\sqrt{s}=2.76\,\mathrm{TeV}$ with a magnetic field of
$0.5\,\mathrm{T}$ . The tool will count for you the total number of
tracks in an event and will publish it to a histogram, from which you
then can read of the mean of the distribution after having analysed
the 30 events.

Additionally you should count the multiplicity in one peripheral, one
semi-central and one central Pb--Pb event and calculate an integrated
$\RAA$ for these events.

For the integrated $\RAA{}$ calculation you need to divide the number
of charged particle tracks in the three Pb--Pb events by 0.6 (why?)
and by the average number of binary collisions $\Ncoll$ given in
Tab.~\ref{tab:ncoll}. 

\begin{table}[htbp]
  \centering
  \begin{tabular}{|lcr|}
    \hline
    \textsf{Classification}
    & \textsf{Centrality class}
    & $\mathbf{N}_{\mathbf{coll}}$
    \\
    \hline
    Peripheral
    & $80-90\%$
    & $6.32$
    \\
    Semi-central
    & $20-40\%$
    & $438.80$
    \\
    Central
    & $0-5\%$
    & $1686.87$
    \\
    \hline
  \end{tabular}
  \caption{Classification of the Pb--Pb events for the visual analysis
    and their corresponding number of binary collisions $\Ncoll$
    according to the Glauber model.}
  \label{tab:ncoll}
\end{table}

\subsection{The tool}

The exercise is done in the ROOT framework, using a simplified version
of the ALICE event display.  To start the tool, run
\begin{quote}
  \ttfamily
  root {\itshape $\langle$installation$\rangle$}/MasterClass.C
\end{quote}
where \texttt{{\itshape $\langle$installation$\rangle$}} is where the
master class package was installed.  (The instructor may have
installed a Desktop launcher you can use instead).

This will bring up a window as shown on the left in
Fig.~\ref{fig:gui:ClassSelector}.  Here, you should choose the
\emph{Nuclear Modification Factor} from the \emph{Select class...}
selector.  Then, to start the first part exercise, select \emph{Part
  1: Visually inspect pp events} from the \emph{Select exercise...}
selector.  This will pop up a new window as shown on the right in
Fig.~\ref{fig:gui:ClassSelector}.  In this window you select your data
set and launch the event display by pressing \textsl{Start} 

\begin{figure}[htbp]
  \centering
  \begin{tabular}{p{.4\linewidth}p{.4\linewidth}}
    \includegraphics[width=\linewidth]{../../Base/doc/ClassSelector}
    & \includegraphics[width=\linewidth]{Part1Selector}
  \end{tabular}
  \caption{Left: Master Class selector.  Chose \emph{Nuclear
      Modification Factor} and \emph{Exercise
      1: Visually inspect pp events}.  Right: Data-set
    selector. Select your data set and press \textsl{Start}}
  \label{fig:gui:ClassSelector}
\end{figure}

All data sets contain
\begin{itemize}
\item 1 pp collision at $\sqrt{s}=7\,\mathrm{TeV}$ recorded without
  any magnetic field.  This serves to illustrate the effect of the
  magnetic field.  You shall \emph{not} count tracks in this event.
\item 30 pp collision at $\sqrt{s}=2.76\,\mathrm{TeV}$ recorded \emph{with}
  any magnetic field.  You shall count the number of charged, primary
  tracks in each of these events by clicking the tracks. 
\item 1 peripheral ($80-90\%$) Pb--Pb event at $\sNN{2.76}$.  You
  shall count the number of charged primary tracks in this event using
  the \textbf{Auto-count} button. 
\item 1 semi-central ($30-40\%$) Pb--Pb event at $\sNN{2.76}$.  You 
  shall count the number of charged primary tracks in this event using
  the \textbf{Auto-count} button. 
\item 1 central ($0-5\%$) Pb--Pb event at $\sNN{2.76}$.  You
  shall count the number of charged primary tracks in this event using
  the \textbf{Auto-count} button. 
\end{itemize}

Once you have selected your data set, hit the \textsl{Start} button.
Note, it may take some time (but no longer than a couple of minutes)
before  something happens: The tool is being build in the background.
Eventually, the event display interface will show up, as shown in
Fig.~\ref{fig:gui:EventDisplay}. 

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\linewidth]{EventDisplay}
  \caption{The event display. Left-hand side shows the event
    navigation interface.  Right-hand side is the 3D event display,
    and two 2D projections.  At the bottom is the tool to count the
    number of charged, primary particles in the events.}
  \label{fig:gui:EventDisplay}
\end{figure}

The column on the left offers a number of options: Instructions, Event
Navigation, display of IP, clusters, and tracks. In addition, under
``Encyclopedia'', there is a brief description of the ALICE detector
and its main components.  At the very bottom are buttons to print the
results to a PDF, export the results to a file, and to exit the tool.

The event display shows three views of ALICE (3-dimensional view,
$r\varphi$ projection and $rz$ projection). You can select the
information displayed for each event. If you click on the relevant
display option, you see all the clusters and tracks of the event.  If
you press the \textit{Show primary only}, then only tracks
corresponding to primary particles will be shown, greatly easing the
task of counting. Once a primary track has been counted by clicking
it, it will turn red to help prevent double counting of tracks.

By clicking on each track the values of the momentum components
$(p_x,p_y,p_z)$ will be shown, and the multiplicity counter will be
incremented.  If the track selected had a $\pT=\sqrt{p_x^2+p_y^2}$
greater than $1\,\mathrm{GeV}$, then the second counter will also be
incremented.

Once you have selected all primary particles in the first 30 pp
collisions, press the button \textbf{Event analysed!} to store the
count in the \textit{Event Characteristics} tab.

\paragraph{Important:} If you change the display settings in the
middle of counting up an event, you will have to start over.

When you get the final three Pb--Pb collisions you will not need to
count each track by hand (you'll quickly see why we find that an
unreasonable task for you to complete).  Instead you hit the
\textbf{Auto-count} button followed by  \textbf{Event analysed!}.

Once you've counted up all 33 events (30 pp, and 3 Pb--Pb), we can
calculate the integrated $\RAA$ for the three Pb--Pb events.  This is
done automatically for you in the tab \textit{Analysis}.  Select that
tab and note down the numbers in the table.  You will then give those
numbers to the instructor (or she may ask you to enter them
yourself).  In the end the instructor will show the results of all
groups and you will discuss them.

\subsection{Collection of results}

Once a data set has been analysed, the numbers for $\RAA$ (with and
without a $\pT$ cut) will be collected by the instructor.  She can
then use the option \emph{Instructor Exercise 1: Collect results} in
the first interface (see Fig.~\ref{fig:gui:ClassSelector}) which will
fire up a special interface for this purpose.  The instructor will
then show the final results to the whole class and you should discuss
these in plenum.

\section{The large scale analysis}
\subsection{The task}

In this exercise of the ALICE master class you will be introduced to a
large scale analysis based on real Pb--Pb data. Your task will be to
implement the extraction of the Pb--Pb transverse momentum spectra in
a given centrality class from a prepared data sample (called a tree),
which contains the centrality of the event, the track multiplicity,
and the transverse momentum of each track. Furthermore, you will use a
program to calculate $\RCP$ (what's that? ask your instructor!) and
$\RAA$ and plot the transverse momentum spectra, the track
multiplicity, the $\RCP$ and $\RAA$ for different centralities in one
plot.

\subsection{Building the transverse momentum spectra}

For this part of the analysis you need to program approximately 10--15
lines of code in a prepared ROOT macro. To start, execute
\begin{quote}
  \ttfamily
  root {\itshape $\langle$installation$\rangle$}/MasterClass.C
\end{quote}
where \texttt{{\itshape $\langle$installation$\rangle$}} is where the
master class package was installed (The instructor may have installed
a Desktop launcher you can use instead).

This will bring up a window as shown on the left in
Fig.~\ref{fig:gui:ClassSelector2}.  Here, you should choose the
\emph{Nuclear Modification Factor} from the \emph{Select class...}
selector.  Then, to start the second exercise, select
\emph{Exercise 2: Batch processing of Pb-Pb} from the \emph{Select
  exercise...}  selector.  This will pop up the spectra fitting tool as
shown on the right in Fig.~\ref{fig:gui:ClassSelector2}.

\begin{figure}[htbp]
  \centering
  \begin{tabular}{p{.32\linewidth}p{.6\linewidth}}
    \includegraphics[width=\linewidth]{../../Base/doc/ClassSelector}
    & \includegraphics[width=\linewidth]{BatchControl}
  \end{tabular}
  \caption{Left: Master Class selector.  Choose \emph{Nuclear
      Modification Factor} and \emph{Exercise
      2: Batch processing of Pb-Pb}.  Right: Tool to edit the code and
    control the batch processing.}
  \label{fig:gui:ClassSelector2}
\end{figure}

In this interface you have several part.  The code you need to edit is
loaded into the \textbf{Editor} tab, while the left hand side allows
you to run the code for different centrality bins, and do the final
calculation of $\RCP$ and $\RAA$.

First, you select the \textbf{Editor} tab.  Here, you will see the
code (C++).  The code is heavily commented (lines starting with
\texttt{//} or text between \texttt{/*...*/}) so that it should be
easier to read.  We will go through the code in the
Section~\ref{sec:code}.  For now, you should take a look at the loop
in lines 45 to 57.  In this loop, we count the number of charged,
primary particles in each event that meets our selection criteria, and
increment the corresponding bin in a histogram.  Next, take a look
that the loop in lines 71 to 81.  This is where you need to add some
code. As you can see, some lines are conspicuously empty --- it is
your job to fill in these lines.  Now go back to lines 45--57 and see
if those lines give you a clue as what to do.

Supposing you have fixed up the code to do the job (and you have saved
the code), you can now try to ``compile'' the code.  This will check
if the code is well-formed and does not contain errors.  Hit the
\textbf{Compile} button (or select
\textit{Tools}$\rightarrow$\textit{Compile Macro} menu-item).  If
all goes well you will see the log of the compilation in the bottom
part of the interface.  If there's an error, you will be notified.  In
case of an error, take a look at the log in the bottom of the
interface and see if you can fix it.  You may need to ask your
instructor for help.

Once the code compiles without errors you can test it out.  Select
\textit{Analyse centrality bin} from the \textit{Select the mode...}
drop-down menu.  Then select a centrality bin of choice from the
\textit{Select centrality bin...} drop-down menu, and finally hit the
\textbf{Go} button.  If all goes well, you will see a plot on the
screen of the multiplicity distribution, multiplicity versus
centrality, and the $\pT$ distribution you filled in in the code (see
left-hand side pf Fig.~\ref{fig:result} for an example).  If there's
some problem in the code, you will see something else, and you should
go back and fix it.

Once you're satisfied that the code works as intended, you should go
through all the centrality bins and run your code on each centrality
bin one--by--one. You will see the same three plots for each
centrality bin (albeit the numbers will be different --- obviously).
You can now use the pre-defined program to calculate $\RAA$ and
$\RCP$, using the $\pT$ distributions your code made in the last pass.
To do that, select \textit{Calculate RAA} from the \textit{Select the
  mode...} drop-down menu, and press the \textbf{Go} button.  You will
see a plot of $\RAA$, $\RCP$, the $\pT$, and multiplicity
distributions for each centrality class, as well as the correlation
between multiplicity and centrality (see right-hand side of
Fig.~\ref{fig:result}).  You will take a moment to reflect on these
results.

\begin{figure}[htbp]
  \centering
  \begin{tabular}{p{.45\linewidth}p{.45\linewidth}}
    \includegraphics[width=\linewidth]{PtSpectra}
    & \includegraphics[width=\linewidth]{Raa}
  \end{tabular}
  \caption{Left: Particle multiplicity, multiplicity versus
    centrality, and $\pT$ spectra in the $30-40\%$ centrality
    bin. Right: Final $\pT$ spectra, $\RAA$, $\RCP$, particle
    multiplicity for each centrality class, as well as the correlation
    between particle multiplicity and centrality.}
  \label{fig:result}
\end{figure}

For the later video conference, you are asked to provide values of
$\RAA$ (with uncertainties) for specific centrality classes and $\pT$
bins.  Your instructor knows which specifically, so ask her for that
information.  To get the numbers, hit the \textbf{Read values} button,
select the centrality and $\pT$ bins, and press \textbf{Fetch} for
each of them.  You should write down the numbers and possibly also
store them in a file using the \textbf{Save} button.  Remember to put
in the name of your class (ask the instructor for it) on the very top. 

\subsection{The code}
\label{sec:code}

In this section we'll go through the code in some detail.

\lstset{%
  language=C++,
  numbers=left,
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\bfseries\color{green!40!black},
  commentstyle=\itshape\color{yellow!40!black},
  stringstyle=\color{orange!40!black},
  morekeywords={Int_t,Float_t,ULong64_t,Double_t},
  classoffset=1,
  keywords={TFile,TTree,TH1,TH2,TCanvas,TVirtualPad},
  keywordstyle=\color{green!20!black},
  classoffset=0,
  emph={pt,centrality,trackTree,ptHist,minCentrality,maxCentrality,ReadEntry,CheckCentrality},
  emphstyle=\color{red!60!black},
}
\def\frag#1#2{%
  \lstinputlisting[showlines,firstnumber=#1,linerange={#1-#2}]{%
    ../scripts/RaaAnalyse.C}}
\frag{1}{14}

In these lines is a file comment explaining briefly what we're looking
at, and some statements to include some functions into this code.  If
you really want to know more, you can take a look at the code in
\texttt{RaaUtilities.C}.

\frag{15}{23}

This is our ``entry point'' --- that is, were the execution starts.
We define a function called \lstinline$RaaAnalyse$ which takes two
arguments
\begin{itemize}
\item \lstinline$minCentrality$ The least centrality to analyse i.e.,
  one of 0, 5, 10, 20, 30, 40, 50, 60, or 70. 
\item \lstinline$maxCentrality$ The largest centrality to analyse i.e.,
  one of 5, 10, 20, 30, 40, 50, 60, 70, or 80. 
\end{itemize}
These numbers are passed in when we select a centrality bin in the
\textit{Select centrality bin...} drop-down menu.

\frag{24}{30}

Next, we customise the style used for plotting
(\lstinline$StyleSettings()$) and then we try to open our input file.
If that fails (\lstinline$!input$) we stop the executing here.

\frag{31}{39}

In this part we declare some variables: \lstinline$nTrack$ and
\lstinline$centrality$ which will contain the number of tracks
(multiplicity) and centrality, respectively, for each event we read
in.  Then we get our tree (data record) of multiplicity
(\lstinline$eventTree$), define a histogram to store the multiplicity
distribution (\lstinline$multHist$), as well as the correlation
between the centrality and multiplicity (\lstinline$multCentHist$).
We also figure out how many events we have in the tree
(\lstinline$nEntries$).

\frag{40}{47}

Here, we start a loop over all events in the multiplicity tree
\lstinline$eventTree$.   The function \lstinline$Progress$ prints a
friendly progress bar.  We will use the variable \lstinline$nEvents$
to count how many events we have analysed --- that is the number of
event with a \lstinline$centrality$ larger than
\lstinline$minCentrality$ and smaller than
\lstinline$maxCentrality$.

\frag{48}{49}

We try to read in the \lstinline$i$ number event (starting from 0)
from the \lstinline$eventTree$ tree.  If that fails, we stop the loop
(\lstinline$break$).

\frag{50}{52}

Next, we use the function \lstinline$CheckCentrality$ to see if the
current events \lstinline$centrality$ is larger than
\lstinline$minCentrality$ and smaller than
\lstinline$maxCentrality$.  If not, then the function returns
\lstinline$false$ and we go on the next event (\lstinline$continue$).

\frag{53}{57}

If the event was accepted by our centrality cut, we fill (count) the
particle multiplicity \lstinline$nTrack$ in the histogram
\lstinline$multHist$, thereby building up the multiplicity
distribution.  We also store the correlation between multiplicity and
centrality in the 2-dimensional histogram \lstinline$multCentHist$.
Finally, we increment (\lstinline$++$) the number of accepted events
(\lstinline$nEvents$). 

\frag{58}{61}

After having loop over all events, we scale our multiplicity
distribution by the number of accepted events creating a distribution
that integrates to unity (1);

\frag{62}{68}

Now we need to set up for the loop over the $\pT$ tree.  We define the
variable \lstinline$pt$ which will contain the transverse momentum of
each track read in.  As before, the variable \lstinline$centrality$
contains the current centrality value.  We get our tree (data record)
of $\pT$ values (\lstinline$trackTree$), define the histogram to build
the $\pT$ spectra in (\lstinline$ptHist$) ad get the number of tracks
to investigate. 

\frag{69}{73}

This is the start of the loop to build the $\pT$ spectra.  Here, we
loop over the number of tracks and print a friendly progress message.

\begin{lstlisting}[showlines,firstnumber=74]
    // Try to read entry from tree . If it fails , stop the loop

    // Check if the read centrality is within range . If not , go on the
    // next entry

    // The centrality is OK , so we fill into our histogram
    
\end{lstlisting}

This is where you need to put in some code.  We won't say more here,
except to say that
\begin{itemize}
\item \lstinline$pt$ contains the current tracks $\pT$
\item \lstinline$centrality$ contains the current centrality
\item \lstinline$ptHist$ is the histogram to fill 
\item Our centrality bin is defined by \lstinline$minCentrality$ and
  \lstinline$maxCentrality$. 
\item The functions \lstinline$ReadEntry$ and
  \lstinline$CheckCentrality$ are useful. 
\end{itemize}

\frag{81}{85}

After having loop over all tracks, we divide our $\pT$ distribution by
the number of accepted events \lstinline$nEvents$ and the width of
each bin, this creating a probability distribution for $\pT$.  Note,
however, that it integrates to the mean multiplicity --- not unity.

The code that follows is largely irrelevant to what you need to do,
but we will briefly describe it here for the sake of completeness.

\frag{86}{94}

We open an output file and store all our results there.  We close the
file so that we cannot tamper with it further.

\frag{95}{106}

Set-up for drawing the results.  A \lstinline$TCanvas$ object is a
``drawing area'' to visualise results in. What follows is what creates
the plot shown in the left-hand side of Fig.~\ref{fig:result}

\frag{107}{124}

Make the left column of the plot, plot the multiplicity distribution
on top, and the correlation between multiplicity and centrality on the
bottom.

\frag{117}{123}

Make the right side of the plot, and plot the $\pT$ distribution.

\frag{125}{137}

Save the whole plot to a PDF file, and end the program. 


\end{document}
