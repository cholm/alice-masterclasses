/**
 * @file   RaaLoad.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar 15 18:52:38 2017
 * 
 * @brief  Load all RAA code for the 1st exercise
 * 
 * 
 */
#include "../../Base/scripts/AliceUtilities.C"

/**
 * Load all RAA code for the 1st exercise
 * 
 * @ingroup alice_masterclass_raa_part1
 */
void
RaaLoad()
{
  TString path = SetMasterClassPath("RaaLoad.C", 3);
  gROOT->Macro(Form("%s/Base/scripts/AliceLoad.C",path.Data())); // 11
  gSystem->AddIncludePath(Form("-I%s/Raa/scripts",path.Data()));
  gROOT->SetMacroPath(Form("%s/Raa/scripts:%s",
			   path.Data(),gROOT->GetMacroPath()));
  gROOT->LoadMacro("RaaHistograms.C+g");   
  gROOT->LoadMacro("RaaCounter.C+g");      
  gROOT->LoadMacro("RaaCalculator.C+g");   
  gROOT->LoadMacro("RaaEventDisplay.C+g"); 
  gROOT->LoadMacro("RaaEditor.C+g");       
  gROOT->LoadMacro("RaaControl.C+g");      
  gROOT->LoadMacro("RaaLog.C+g");          
  gROOT->LoadMacro("RaaExercise.C+g");     
}
//
// EOF
//
