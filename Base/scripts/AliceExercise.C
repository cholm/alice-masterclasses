/**
 * @file   AliceExercise.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar  8 10:22:41 2017
 * 
 * @brief  Base class for exercises 
 * 
 * @ingroup  alice_masterclass_base
 */
#ifndef ALICEEXERCISE_C
#define ALICEEXERCISE_C
#ifndef __CINT__
# include <TRootBrowser.h>
# include <TApplication.h>
# include <TSystem.h>
# include <TFile.h>
# include "AliceUtilities.C"
# ifdef R__UNIX
#  include <unistd.h>
# endif
#else
class TRootBrowser;
class TFile;
#endif

/** 
 * This defines the interface for exercises.  A specific exercise
 * should implement this interface to set up the GUI elements needed
 * by the exercise
 * 
 * @ingroup alice_masterclass_base 
 */
class AliceExercise
{
public:
  /** 
   * Constructor
   * 
   */
  AliceExercise() {}
  /** 
   * Set-up the elements needed by the exercise - e.g., GUI elements
   * or the like
   * 
   * @param browser The browser we're using 
   * @param cheat   If true, set-up for cheating 
   */
  virtual void Setup(TRootBrowser* browser,Bool_t cheat) = 0;
  /** 
   * Toggle cheats
   * 
   * @param on if true, cheats are on
   */
  virtual void ToggleCheat(Bool_t on) {}
  /** 
   * Get location of instructions file
   */
  virtual const char* InstructionsFile() const { return ""; }
  /** 
   * Return true if we can cheat
   */
  virtual Bool_t CanCheat() const { return true; }
  /** 
   * Return true if we can save 
   */
  virtual Bool_t CanSave() const { return false; }
  /** 
   * Return true if we can print 
   */
  virtual Bool_t CanPrint() const { return false; }
  /** 
   * Default basename for exported PDFs and ROOT files
   * 
   * @return Here, the string @e export 
   */
  virtual const char* DefaultBase() const { return "export"; }
  /** 
   * Print results to a PDF 
   *
   * @param out Output file name
   */
  virtual void PrintPdf(const char* out) {}
  /** 
   * Export results to external file
   *
   * @param out Output file name
   */
  virtual void Export(const char* out) {}
  /** 
   * Exit the exercise - default is to quit ROOT completely 
   * 
   */
  virtual void Exit()
  {
    gApplication->Terminate();
  }
  /** 
   * Run the exercise in auto-mode
   * 
   */
  virtual void Auto() {}
  /**
   * Hook to run after auto analysis 
   */
  virtual void PostAuto() {}
  /** 
   * Return the path to data relative to the top-level directory 
   * 
   * @return Path to data, relative to top-level directory 
   */
  virtual const char* DataDir() const = 0;
  /**
   * Open a data file. 
   *
   * @param filename The file name in the data directory 
   * @param dir      (Optional) directory (relative to top) 
   *
   * @return Pointer to file instance or null if not found. 
   */
  virtual TFile* DataFile(const char* filename,
			  const char* dir=0) const
  {
    return AliceFile((dir ? dir : DataDir()), filename);
  }
};
#endif
//
// EOF
//

