/**
 * @file   AliceLoad.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar 15 19:48:18 2017
 * 
 * @brief  Load base classes
 * 
 * @ingroup  alice_masterclass_base
 */

/** 
 * Script to load all the code 
 * 
 * @ingroup alice_masterclass_base
 */
void
AliceLoad()
{
  TString path("./");
  if (gSystem->Getenv("ALICE_MASTERCLASS"))
    path = gSystem->ExpandPathName("$ALICE_MASTERCLASS/");
  
  // gSystem->Exec("rm -vf Base/*_C.*");
  gSystem->AddIncludePath(Form("-I%sBase/scripts",path.Data()));
  gROOT->SetMacroPath(Form("%sBase/scripts:%s",
			   path.Data(),gROOT->GetMacroPath()));
  gROOT->LoadMacro("AliceUtilities.C+g");
  gROOT->LoadMacro("AliceExercise.C+g");
  gROOT->LoadMacro("AliceInstructions.C+g");
  gROOT->LoadMacro("AliceBrowser.C+g");
  gROOT->LoadMacro("AliceDetails.C+g");  
  gROOT->LoadMacro("AliceInfo.C+g");
  gROOT->LoadMacro("AliceLogoButtons.C+g");
  gROOT->LoadMacro("AliceNavigation.C+g");
  gROOT->LoadMacro("AliceEventDisplay.C+g");
  gROOT->LoadMacro("MultiView.C+g");
  gROOT->LoadMacro("AliceVSDReader.C+g");
}
//
// EOF
//
