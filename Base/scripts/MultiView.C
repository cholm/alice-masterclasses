/**
 * @file   Base/scripts/MultiView.C
 * @author Matevz Tadel
 * @date   2009
 * 
 * @brief Multi-view (3d, rphi, rhoz) service class using EVE Window
 * Manager.
 * 
 * @ingroup  alice_masterclass_base_eventdisplay
 * 
 */


#ifndef MULTIVIEW_C
#define MULTIVIEW_C
#ifndef __MAKECINT__
// # include <TEveElement.h>
# include <TEveManager.h>
# include <TEveViewer.h>
# include <TGLViewer.h>
# include <TEveScene.h>
# include <TEveProjectionManager.h>
# include <TEveProjectionAxes.h>
# include <TEveBrowser.h>
# include <TEveGeoShape.h>
#else
class TEveElement;
class TEveProjection;
class TEveProjectionAxes;
class TEveProjectionManager;
class TEveViewer;
class TEveScene;
class TEveGeoShape;
#endif

/**
 * MultiView
 *
 * Structure encapsulating standard views: 3D, @f$(r,\phi)@f$ and
 * @f$(\rho,z)@f$.  Includes scenes and projection managers.
 *
 * Should be used in compiled mode.
 *
 * @ingroup  alice_masterclass_base_eventdisplay
 */
struct MultiView
{
  TEveProjectionManager *fRPhiMgr;
  TEveProjectionManager *fRhoZMgr;
  TEveViewer            *f3DView;
  TEveViewer            *fRPhiView;
  TEveViewer            *fRhoZView;
  TEveScene             *fRPhiGeomScene;
  TEveScene             *fRhoZGeomScene;
  TEveScene             *fRPhiEventScene;
  TEveScene             *fRhoZEventScene;

  TEveGeoShape          *fGeomGentle;     // Obvious meaning.
  TEveGeoShape          *fGeomGentleRPhi; // Obvious meaning.
  TEveGeoShape          *fGeomGentleRhoZ; // Obvious meaning.


  //---------------------------------------------------------------------------
  /**
   * Constructor --- creates required scenes, projection managers and
   * GL viewers.
   * 
   */
  MultiView()
  {
    // --- Scenes ----------------------------------------------------
    fRPhiGeomScene  = gEve->SpawnNewScene("RPhi Geometry",
					  "Scene holding projected geometry "
					  "for the RPhi view.");
    fRhoZGeomScene  = gEve->SpawnNewScene("RhoZ Geometry",
					  "Scene holding projected geometry "
					  "for the RhoZ view.");
    fRPhiEventScene = gEve->SpawnNewScene("RPhi Event Data",
					  "Scene holding projected event-data "
					  "for the RPhi view.");
    fRhoZEventScene = gEve->SpawnNewScene("RhoZ Event Data",
					  "Scene holding projected event-data "
					  "for the RhoZ view.");

    // --- Projection managers ---------------------------------------
    fRPhiMgr = new TEveProjectionManager(TEveProjection::kPT_RPhi);
    gEve->AddToListTree(fRPhiMgr, kFALSE);
    TEveProjectionAxes* a = new TEveProjectionAxes(fRPhiMgr);
    a->SetMainColor(kWhite);
    a->SetTitle("R-Phi");
    a->SetTitleSize(0.05);
    a->SetTitleFont(102);
    a->SetLabelSize(0.025);
    a->SetLabelFont(102);
    fRPhiGeomScene->AddElement(a);
    
    fRhoZMgr = new TEveProjectionManager(TEveProjection::kPT_RhoZ);
    gEve->AddToListTree(fRhoZMgr, kFALSE);
    a = new TEveProjectionAxes(fRhoZMgr);
    a->SetMainColor(kWhite);
    a->SetTitle("Rho-Z");
    a->SetTitleSize(0.05);
    a->SetTitleFont(102);
    a->SetLabelSize(0.025);
    a->SetLabelFont(102);
    fRhoZGeomScene->AddElement(a);
    
    // --- Viewers ---------------------------------------------------
    TEveWindowSlot *slot = 0;
    TEveWindowPack *pack = 0;

    slot = TEveWindow::CreateWindowInTab(gEve->GetBrowser()->GetTabRight());
    pack = slot->MakePack();
    pack->SetElementName("Multi View");
    pack->SetHorizontal();
    pack->SetShowTitleBar(kFALSE);
    pack->NewSlot()->MakeCurrent();
    f3DView = gEve->SpawnNewViewer("3D View", "");

    f3DView->AddScene(gEve->GetGlobalScene());
    f3DView->AddScene(gEve->GetEventScene());

    pack = pack->NewSlot()->MakePack();
    pack->SetShowTitleBar(kFALSE);
    pack->NewSlot()->MakeCurrent();
    fRPhiView = gEve->SpawnNewViewer("RPhi View", "");
    fRPhiView->GetGLViewer()->SetCurrentCamera(TGLViewer::kCameraOrthoXOY);
    fRPhiView->AddScene(fRPhiGeomScene);
    fRPhiView->AddScene(fRPhiEventScene);

    pack->NewSlot()->MakeCurrent();
    fRhoZView = gEve->SpawnNewViewer("RhoZ View", "");
    fRhoZView->GetGLViewer()->SetCurrentCamera(TGLViewer::kCameraOrthoXOY);
    fRhoZView->AddScene(fRhoZGeomScene);
    fRhoZView->AddScene(fRhoZEventScene);
  }
  /** 
   * Set current depth on all projection managers.
   * 
   * @param d Depth
   */
  void SetDepth(Float_t d)
  {
    fRPhiMgr->SetCurrentDepth(d);
    fRhoZMgr->SetCurrentDepth(d);
  }
  /** 
   * Initialize gentle geometry.
   * 
   * @param g3d     Shape for 3D view
   * @param grphi   Shape for @f$(r,\phi)@f$ projection
   * @param grhoz   Shape for rho z projection
   */
  void InitGeomGentle(TEveGeoShape* g3d, TEveGeoShape* grphi, TEveGeoShape* grhoz)
  {
    fGeomGentle     = g3d;
    fGeomGentleRPhi = grphi; fGeomGentleRPhi->IncDenyDestroy();
    fGeomGentleRhoZ = grhoz; fGeomGentleRhoZ->IncDenyDestroy();

    ImportGeomRPhi(fGeomGentleRPhi);
    ImportGeomRhoZ(fGeomGentleRhoZ);
  }
  /** 
   * Import @f$(r,\phi)@f$ projection
   * 
   * @param el Element to project
   */
  void ImportGeomRPhi(TEveElement* el) { fRPhiMgr->ImportElements(el, fRPhiGeomScene); }
  /** 
   * Import @f$(\rho,z)@f$ projection
   * 
   * @param el Element to project
   */
  void ImportGeomRhoZ(TEveElement* el) { fRhoZMgr->ImportElements(el, fRhoZGeomScene); }
  /** 
   * Import @f$(r,\phi)@f$ event 
   * 
   * @param el Element to import 
   */
  void ImportEventRPhi(TEveElement* el) { fRPhiMgr->ImportElements(el, fRPhiEventScene); }
  /** 
   * Import @f$(\rho,z)@f$ event 
   * 
   * @param el Element to import 
   */
  void ImportEventRhoZ(TEveElement* el) { fRhoZMgr->ImportElements(el, fRhoZEventScene); }
  /** 
   * Remove @f$(r,\phi)@f$ event
   */
  void DestroyEventRPhi() { fRPhiEventScene->DestroyElements(); }
  /** 
   * Remove @f$(\rho,z)@f$ event 
   */
  void DestroyEventRhoZ() { fRhoZEventScene->DestroyElements(); }
  /**
   * Remove @f$(r,\phi)@f$ projection
   */
  void DestroyGeomRPhi() { fRPhiGeomScene->DestroyElements(); }
  /**
   * Remove @f$(\rho,z)@f$ projection
   */
  void DestroyGeomRhoZ() { fRhoZGeomScene->DestroyElements(); }
  /** 
   * Remove all projections
   */
  void DestroyAllGeometries()
  {
    fGeomGentle->DestroyElements();
    fGeomGentleRPhi->DestroyElements();
    fGeomGentleRhoZ->DestroyElements();
  }
  /** 
   * Make axes for @f$(r,\phi)@f$ projection
   */
  void SetRPhiAxes()
  {
    gEve->AddToListTree(fRPhiMgr, kFALSE);
    TEveProjectionAxes* a = new TEveProjectionAxes(fRPhiMgr);
    a->SetMainColor(kWhite);
    a->SetTitle("R-Phi");
    a->SetTitleSize(0.05);
    a->SetTitleFont(102);
    a->SetLabelSize(0.025);
    a->SetLabelFont(102);
    fRPhiGeomScene->AddElement(a);
  }
  /** 
   * Make axes for @f$(\rho,z)@f$ projection
   */
  void SetRhoZAxes()
  {
    gEve->AddToListTree(fRhoZMgr, kFALSE);
    TEveProjectionAxes* a = new TEveProjectionAxes(fRhoZMgr);
    a->SetMainColor(kWhite);
    a->SetTitle("Rho-Z");
    a->SetTitleSize(0.05);
    a->SetTitleFont(102);
    a->SetLabelSize(0.025);
    a->SetLabelFont(102);
    fRhoZGeomScene->AddElement(a);
  }
  /** 
   * Get the 3D view
   * 
   * @return Pointer to view 
   */
  TEveViewer* Get3DView() { return f3DView; }
  /** 
   * Get the @f$(r,\phi)@f$ view
   * 
   * @return Pointer to view 
   */
  TEveViewer* GetRPhiView() { return fRPhiView; }
  /** 
   * Get the @f$(\rho,z)@f$ view
   * 
   * @return Pointer to view 
   */
  TEveViewer* GetRhoZView() { return fRhoZView; }
  /** 
   * Get the @f$(r,\phi)@f$ event 
   * 
   * @return The scene 
   */
  TEveScene* GetRPhiEventScene() { return fRPhiEventScene; }
  /** 
   * Get the @f$(\rho,z)@f$ event 
   * 
   * @return The scene 
   */
  TEveScene* GetRhoZEventScene() { return fRhoZEventScene; }
};
#endif
//
// EOF
//
