/**
 * @file   TestBrowser.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Aug 30 23:20:25 2017
 * 
 * @brief  A test of the browser 
 */
void TestBrowser()
{
  gROOT->Macro("Base/scripts/AliceLoad.C");
  gROOT->LoadMacro("Base/scripts/TestExercise.C+g");

  AliceBrowser* browser = new AliceBrowser("A test");
  browser->Setup(new TestExercise);
  new TBrowser("b", "b", browser);
}

  
//
// EOF
//
