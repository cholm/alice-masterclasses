/**
 * @file   TestExercise.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Aug 30 23:20:45 2017
 * 
 * @brief  A test of an exercise
 */

#include "AliceExercise.C"
#include "AliceLogoButtons.C"
#include <TRootBrowser.h>
#include <TCanvas.h>

struct TestControl : public TGMainFrame
{
  TestControl(AliceExercise* e)
    : TGMainFrame(gClient->GetRoot(), 0, 0)
  {
    new AliceLogoButtons(this, e);
    MapSubwindows();
    Resize(GetDefaultSize());
    MapWindow();
  }
  ClassDef(TestControl,0);
};
    
struct TestExercise : public AliceExercise
{
  const char* InstructionsFile() const { return "doc/Description.html"; }
  void Setup(TRootBrowser* browser)
  {
    browser->StartEmbedding(TRootBrowser::kLeft);
    new TestControl(this);
    browser->StopEmbedding("Control");

    browser->StartEmbedding(TRootBrowser::kRight);
    new TCanvas("canvas");
    browser->StopEmbedding("Canvas");
  }
};
//
// EOF
//
