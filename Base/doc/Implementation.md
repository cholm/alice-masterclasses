Notes on the implementation
===========================

- The event display of the two master classes have been unified. The
  master class specific stuff is handled by classes deriving from
  `AliceEventDisplay` (e.g., `RaaEventDisplay.C`). 
  
  The base code for the event display, etc. lives in the sub-directory
  `Base`. Here, we have classes for showing instructions, event
  navigation, etc.  Note, the loading of data for the event display
  have been separated out from the various GUI elements. 
  
  Typically, these Master Class specific sub-classes uses additional
  code specific to the master class.  For example, for the RAA
  exercise we have the classes 
  
  - `RaaEventDisplay` which defines the interface and what data to load. 
  - `RaaHistograms` which contain our histograms
  - `RaaCalculator` which knows how to calculate RAA
  - `RaaCounter` which provides an interface to count charged particle
    tracks. 
	
- For the second part of the exercises, we use a `TBrowser` based
  GUI.  That is, we define have the class `AliceBrowser` which uses
  sub-classes of `AliceExercise` for setting up the GUI.  For example,
  for the strangeness exercise we have `StrangeExercise` which takes
  care of setting up the interface (via embedded frames) and the
  actual calculations.  That is, we have 
  
  - `StrangeExercise` - which defines the interface and does the
    calculations. 
  - `StrangePicker` - which defines the tree of invariant mass
    distributions to fit. 
  - `StrangeFitter` - which allows us to limit the range of the fit
    as well as the location of the peak. 
  
- The ALICE logo button typically toggle "cheats".  E.g., for the RAA
  class, in the first exercise, toggling the logo button will toggle
  auto-counting of tracks.  This is done so it's easier for the
  instructor to check out things etc. 
  
- The code is implemented so that it can be installed where ever you
  want (useful is the students do not have write access in the project
  directory).  So you can put it in say /opt/alice/masterclasses for
  example. 
  
  The students should then execute 
  
     root /opt/alice/masterclasses/MasterClass.C 
	 
  in their home directory or some sub-directory thereof.  All files
  will be created in that directory.  Note, there's a .desktop file
  that can be put in the desktop.  Just make sure that you change the
  paths to the `root` executable and to the `MasterClass.C` script. 
  
- The code works with ROOT versions 5.34 and 6 without any additional
  add-ons. 
  
- The code should work on all platforms supported by ROOT 

- New master classes can be added following the templates given in the
  `Raa` and `Strangeness` sub-directories.  
  
  The top-level GUI will automatically detect the presence of master
  classes if the script `Class.C` is present in the sub-directory.
  That script is supposed to return a `TList` with key-value pairs
  (`TNamed` objects) in a tree like 
  
	  . -+- name (Name of master class)
	     |
		 +- desc (File path to HTML description)
		 |
		 +- exer (A `TList` of exercises)
		    |
			+ name of exercise (title is script to execute)
			|
			+ ...
			
  The `TNamed` objects in the `exer` list will give the name of the
  exercise, and (as the title) the script to execute relative to the
  top directory).  Note, the script _must_ accept a single boolean
  parameter which selects _cheat_ mode if available.
  
  Should the master class need the event display, it should define a
  class that derives from `AliceEventDisplay.` 
  
  Other exercises should be specified via sub-classes of
  `AliceExercise`.
  
EOF
