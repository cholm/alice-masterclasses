\documentclass[11pt,twoside]{article}

\usepackage[margin=2cm,a4paper]{geometry}
\usepackage[danish]{babel}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{amstext}
\usepackage{amsmath}
\usepackage{hyperref}
% \def\includegraphics[#1]#2{}

\title{At finde ``mærkelige'' partikler}
\author{ALICE Collaboration}
\date{\today}
\setlength{\parindent}{0pt}
\setlength{\parskip}{1ex}
\def\strange{\emph{strange}}
\def\enhancement{\emph{strangeness enhancement}}
\def\confinement{\emph{confinement}}
\def\deconfined{\emph{deconfined}}
\def\KZeroS{\ensuremath \mathrm{K}^0_{\mathrm{S}}}
\def\pion#1{\ensuremath \mathrm{\pi}^{#1}}
\def\proton{\ensuremath \mathrm{p}}

\begin{document}
\thispagestyle{empty}
\newdimen\tempdima
\tempdima=\textwidth
\advance\tempdima-4.5cm
{\makeatletter\xdef\ddate{\@date}}
\begin{tabular}[t]{@{}p{2cm}@{}p{\tempdima}@{}p{2cm}@{}}
  \multicolumn{3}{c}{\Large EUROPEAN ORGANIZATION FOR NUCLEAR RESEARCH}\\[8mm]
  \includegraphics[height=2cm]{../../Base/doc/ALICE_logo}
  &\hfill
  &\includegraphics[height=2cm]{../../Base/doc/CERN_logo}\\[3mm]
  &
  \multicolumn{2}{r}{\ddate}
\end{tabular}
\begin{center}
  {\makeatletter\Large\textbf{\@title}}
  \par
  \vspace*{5mm}
  \begin{tabular}[t]{c}
    {\makeatletter\large\@author}
  \end{tabular}
\end{center}
{\let\thempfn\empty
  \footnotetext{\hspace*{-1.5\parindent}\noindent\normalsize%
    \copyright\,\the\year\ CERN til fordel for ALICE
    kollaborationen.\newline%
    Reproduktion af denne artikel eller dele deraf er tilladt som
    angivet i \href{http://creativecommons.org/licenses/by/4.0}{CC-BY-4.0
      licensen}.}}
\tableofcontents

\section{Oversigt}

Denne Master Class består af eftersøgningen efter ``mærkelige''
(\strange) partikler produceret i kollisioner ved LHC og registreret
af ALICE. Eftersøgningen er baseret på \strange{} partiklers
henfaldsmønstre kendt som ``V0-decays'', såsom
\begin{align*}
  \KZeroS &\rightarrow\pion++\pion-\\
  \Lambda &\rightarrow \proton + \pion-\\
  \intertext{og kaskader som}
  \Xi^{-}&\rightarrow \Lambda + \pion-\quad,
\end{align*}
hvor $\Lambda$ yderligere henfalder til en proton og en negativt ladet
pion ($\Lambda \rightarrow\proton +\pion-$), så vi har de endelige
henfaldsprodukter $\proton+\pion-+\pion-$. Identifikationen af 
\strange{} partikler er baseret på topologien af deres henfald kombineret
med identifikationen af henfaldsprodukterne; Oplysningerne fra sporene
bruges til at beregne den invariante masse af den henfaldende partikel
som en yderligere bekræftelse af den henfaldende partikeltype.

I det følgende beskrives ALICE og dets fysiske mål først kort, og
derefter følger fysik motivation for denne analyse. Metoden til
identifikation af \strange{} partikler såvel som redskaber er
beskrevet detaljeret; og alle trin i øvelsen forklares udførligt. Til
sidst præsenteres en store-skala analyse.

\section{Introduktion}

ALICE (A Large Ion Collider Experiment) er en af de fire store
eksperimenter ved CERNs Large Hadron Collider (LHC), og er designet
til at studere tung-ion kollisioner. ALICE studerer også proton-proton
kollisioner, der \emph{primært} tilvejebringer referencedata for
resultater fra tung-ions kollisionerne. Proton-proton kollisionsdata
en giver dog også mulighed for en række proton-proton fysik
undersøgelser. ALICE er designet til at klare de højeste partikel
multipliciteter (antal), der forventes for kollisioner af bly kerner
ved LHCs ekstreme energier.

\section{ALICE Fysik}

Kvarker bindes sammen i protoner og neutroner med en kraft kendt som
den stærke vekselvirkning, medieret af udveksling af
kraft-bærer-partikler kaldet gluoner. Den stærke vekselvirkninger er
også ansvarlig for at binde protonerne og neutronerne inde i
atomkernen.

Selvom vi ved at kvarker er elementære partikler, der opbygger alle
kendte hadroner, er ingen kvarker nogensinde blevet observeret
isoleret: kvarkerne såvel som gluonerne synes at være bundet sammen og
indesluttet inden for kompositpartikler, såsom protoner og
neutroner. Dette er kendt som \emph{indespærring}
(\confinement). Den nøjagtige mekanisme, der forårsager dette,
er ukendt.

Selvom meget af fysikken i stærke vekselvirkninger i dag er godt
forstået, er der to meget grundlæggende spørgsmål som er uløste:
oprindelsen af \confinement{} og mekanismen for generering af
masse. Begge menes at skyldes den måde, hvorpå vakuumets egenskaber
modificeres ved af stærk vekselvirkninger.

Den nuværende teori om den stærke vekselvirkning (kaldet Quantum
Chromo-Dynamics) forudsiger at kvarker og gluoner ikke længere
begrænses indeni kompositpartikler ved meget høje temperaturer eller
tætheder - de er \deconfined{}. I stedet eksistere de frit i en ny
tilstand af materie kendt som \emph{kvark-gluon plasma} (Quark-Gluon
Plasma).

En sådan overgang er forudsagt at ske når temperaturen overstiger en
kritisk værdi der anslås at være ca.\ $100\,000$ gange varmere end i
solens kerne. Sådanne temperaturer har ikke eksisteret i naturen siden
universets fødsel. Vi mener at et par milliontedele af et sekund efter
Big Bang var temperaturen faktisk over den kritiske værdi, og hele
universet var i en kvark-gluon plasma tilstand.

Når to tunge kerner nærmer hinanden med hastigheder tæt på lysets
hastighed og kolliderer kan disse ekstreme temperature og betingelser
genskabes og frigøre kvarker og gluoner. Kvarker og gluoner kolliderer
med hinanden og skaber et termisk ækvilibreret miljø: kvark-gluon
plasmaen. Denne plasma udvider og køler ned til temperaturen
($10^{12}{}^{\circ}$), hvor kvarker og gluoner omgruppere sig til
almindeligt stof, knap $10^{-23}$ sekunder efter kollisionen. ALICE
vil studere dannelsen og egenskaberne af denne nye tilstand af
materie.

\section{Forhøjet ``mærkelighed'' som en tegn på kvark-gluon plasma}

Diagnosen og undersøgelsen af egenskaberne af kvark-gluon plasma (QGP)
kan udføres ved at anvende at kvarker som ikke til findes i
almindeligt stof omkring os. En af de eksperimentelle signaturer er
afhængig af ideen om øget ``mærkelighed'' (\enhancement{}). Dette var
en af de første observable signaturer for kvark-gluon plasma,
foreslået i 1980. I modsætning til op- og ned-kvarkene er der ikke
bragt \strange{} kvarker ind i reaktionen mellem de kolliderende
kerner. Derfor er eventuelle \strange{} kvarker eller
anti-kvarker\footnote{I det følgende vil vi sige \strange{} kvarker og
  mene \emph{både} \strange{} kvarker og anti-kvarker for en nemheds
  skyld, med mindre at forskellen er væsentligt.}, der observeres i
forsøget, blevet fremstillet helt ``friske'' fra den kinetiske energi
af de kolliderende kerner. Heldigt nok svarer massen af \strange{}
kvarker til den temperatur eller energi, hvorved protoner, neutroner
og andre hadroner opløses i kvarker. Det betyder, at antallet af
\strange{} kvarker er følsomme for forholdene, strukturen og
dynamikken i den \deconfined{} tilstand (fase), og hvis antallet er
store kan det tyde på at betingelserne for ``frie'' kvarker og gluoner
er nået.

I praksis kan \enhancement{} observeres ved at tælle antallet
af \strange{} partikler ---  partikler indeholder mindst \'en
\strange{} kvark og beregning af forholdet mellem \strange{} og
ikke-\strange{} partikler. Hvis dette forhold er højere end det, der
er givet af de teoretiske modeller, der ikke forudser en   QGP, så er 
forhøjelsen af ``mærkelighed'' observeret (\enhancement). 

Alternativt normaliseres antallet af \strange{} partikler til antallet
af nukleoner, der deltager i interaktionen og sammenlignes med samme
forhold for proton-proton kollisioner

\section{``Mærkelige'' partikler}

\emph{Strange} partikler er hadroner, der indeholder mindst en
\strange{} kvark. Dette er kendetegnet ved kvante-tallet
``mærkværdighed'' (\emph{strangeness}). Den letteste neutrale
\strange{} meson er
$\KZeroS$ ($\mathrm{d}\bar{\mathrm{s}}$)%
og den letteste neutrale \strange{} baryon er
$\Lambda$ (uds), karakteriseret som \emph{hyperon}.

Her studerer vi deres henfald, for eksempel
$\KZeroS\rightarrow \pion++\pion-$, %
$\Lambda\rightarrow \proton + \pion-$.  %
I disse henfald er kvante-tallet \emph{strangeness} er \emph{ikke}
bevaret, da henfalds-produkterne kun består af op- og
ned-kvarker. Derfor er disse ikke stærke henfald (som ville ske
ekstremt hurtigt med halveringstid $\tau \approx 10^{-23}\mathrm{s}$) men
\emph{svage} henfald, hvor \emph{strangeness} kan bevares
($\Delta S= 0$) eller ændres med 1 ($\Delta S = 1$). For disse nedfald
er halveringstiden $\tau$ mellem $10^{-8}\mathrm{s}$ og
$10^{-10}\mathrm{s}$. For partikler, der rejser tæt på lysets
hastighed betyder det, at partiklen henfalder (i gennemsnit) nogle
centimeter fra produktionspunktet (f.eks.\ fra kollisionsstedet).

\section{Hvordan vi finder ``mærkelige'' partikler}

Formålet med øvelsen er at søge efter \strange{} partikler produceret
i kollisioner ved LHC og målt med ALICE.

Som nævnt i det foregående afsnit lever \strange{} partikler ikke længe;
de henfalder kort tid  efter (eller ikke langt fra) deres
produktion. De lever imidlertid længe nok til at rejse nogle
centimeter fra interaktionspunktet (IP), hvor de blev produceret
(primært vertex). Eftersøgningen efter \strange{} partikler er således
baseret på identifikation afderes henfaldsprodukter, som skal stamme fra et
fælles sekundært vertex.

Neutrale \strange{} partikler, såsom $\KZeroS$ og
$\Lambda$ henfald giver et karakteristisk henfaldsmønster, kaldet
V0. Moder partikelen forsvinder nogle centimeter fra
interaktionspunktet og to modsat ladet partikler viser sig i stedet.
Disse to ladet partikler afbøjes i modsatte retninger i ALICEs magnetfeltet.

De henfald, vi vil lede efter, er vist i Fig.~\ref{fig:v0_patterns}
\begin{figure}[htbp]
  \centering
  \begin{tabular}{p{.45\linewidth}p{.195\linewidth}p{.28\linewidth}}
    \includegraphics[width=\linewidth]{../../Base/doc/kaon}
    & \includegraphics[width=\linewidth]{../../Base/doc/lambda}
    & \includegraphics[width=\linewidth]{../../Base/doc/antilambda}
    \\
    $\KZeroS\rightarrow\pion++\pion-$
    & $\Lambda\rightarrow\proton+\pion-$
    & $\bar{\Lambda}\rightarrow\bar{\proton}+\pion+$
  \end{tabular}
  \caption{Henfaldsmønstre af $\mathrm{K}^S_0$, $\Lambda$, og
    $\bar{\Lambda}$.  Røde spor er positivt ladet partikler, mens
    grønne spor er negativt ladet partikler.}
  \label{fig:v0_patterns}
\end{figure}

Vi ser at for sluttilstanden $\pion++\pion-$ er decay-mønsteret
kvasi-symmetrisk, mens i $\pion-+\proton$ (og $\pion++\bar\proton$)
sluttilstanden er (anti-)protonens krumningsradius er større end den
for $\pion\pm$: på grund af dens højere masse bærer proton det meste af det
oprindelige impuls.

Vi vil også være på udkig efter kaskade henfald af ladet
\strange{} partikler, såsom $\Xi^{-}$ som henfalder til $\pion-$ og
$\Lambda$; hvor  $\Lambda$ derefter henfalder til $\pion-$ og p; den
oprindelige $\pi$ er karakteriseret som en bachelor (enkel ladet spor)
og vises i lilla. Topologien er vist i  Fig.~\ref{fig:cascade_pattern}.
\begin{figure}[htbp]
  \centering
  \begin{tabular}{p{.45\linewidth}}
    \includegraphics[width=\linewidth]{../../Base/doc/xi}
    \\
    $\Xi^-\rightarrow\pion-+\Lambda(\rightarrow\proton+\pion-)$
  \end{tabular}
  \caption{Henfaldsmønster for en $\Xi^-$. Ladet positive og negative
    spor er henholdsvis røde og grønne, mens bachelor spor er lilla.}
  \label{fig:cascade_pattern}
\end{figure}

Søgningen efter V0'er er baseret på henfalds-topologien og
identifikationen af henfaldsprodukterne. En yderligere bekræftelse af
partikelidentiteten er beregningen af   dens masse. Dette gøres ud fra
information (masse og impuls) af henfaldsprodukterne som beskrevet i
det følgende afsnit.

\section{Den invariante masseberegning}
\label{sec:invmass}

Vi betragter henfaldet af en neutral kaon til to ladede pioner
$$
\KZeroS\rightarrow\pion++\pion-
$$
Lad $E$, $\mathbf{p}=(p_x,p_y,p_z)$ og $m$ være moder partiklen
($\KZeroS$) henholdsvis energi, impuls (en
vektor), og masse.  Tilsvarende lader vi $E_1$,
$\mathbf{p}_1=(p_{1,x},p_{1,y},p_{1,z})$ og $m_1$ være energien,
impulsen, og massen af det første henfaldsprodukt ($\pion+$), og $E_2$,
$\mathbf{p}_2=(p_{2,x},p_{2,y},p_{2,z})$ og $m_2$ være energien,
impulsen, og massen af det andet henfaldsprodukt ($\pion-$). Fra
bevarelse af energi og impuls har vi henholdsvis

\begin{align*}
  E & = E_1+E_2\\
  \mathbf{p} &= \mathbf{p}_1+\mathbf{p}_2\quad.
\end{align*}
Fra speciel relativitet (hvor vi sætter lysets hastighed $c=1$) har vi
at
\begin{align*}
  E^2
  &= p^2 + m^2
  & E_1^2
  &=p_1^2+m_1^2
  & E_2^2
  &=p_2^2+m_2^2\\
  p&=\sqrt{p_x^2+p_y^2+p_z^2}
  & p_1
  &=\sqrt{p_{1,x}^2+p_{1,y}^2+p_{1,z}^2}
  & p_2
  &=\sqrt{p_{2,x}^2+p_{2,y}^2+p_{2,z}^2}\quad,
\end{align*}
hvor $p$, $p_1$, og $p_2$ er længderne af impulsvektorene.  Fra dette
finder vi at
\begin{align*}
  m^2 &= E^2 - p^2 = (E_1+E_2)^2 - (\mathbf{p}_1+\mathbf{p}_2)^2 \\
      &= E_1^2+E_2^2 +2E_1E_2
        - \mathbf{p}_1\cdot\mathbf{p}_1
        - \mathbf{p}_2\cdot\mathbf{p}_2
        - 2\mathbf{p}_1\cdot\mathbf{p}_2\\
  \intertext{For enhver vektor $\mathbf{v}\cdot\mathbf{v}=v$, så}
      &= E_1^2+E_2^2+2E_1E_2 - p_1^2-p_2^2 -
        2\mathbf{p}_1\cdot\mathbf{p}_2\\
  \intertext{og for 3-dimensionalle vektorer
  $\mathbf{v}\cdot\mathbf{u}=v_xu_x+v_yu_y+v_zu_z$, har vi} 
      &= E_1^2+E_2^2+2E_1E_2 - p_1^2-p_2^2 -
        2\left(p_{1,x}p_{2,x}+p_{1,y}p_{2,y}+p_{1,z}p_{2,z}\right)\quad.
\end{align*}
Vi kan derfor beregne massen af den oprindelige partikel fra masserne
og impulserne af datter-partiklerne. Masserne af 
datter-partiklerne $m_1$ og $m_2$ er kendte: forskellige
detektorer i ALICE identificerer partikler.

Impulserne $\mathbf{p}_1$ og $\mathbf{p}_2$ af datter-partiklerne
findes ved at måle krumningsradiusen af deres bane på i det kendte
magnetfelt. I øvelsen bruger vi de tre komponenter af impuls vektorene af
hvert spor forbundet med V0-forfaldet, som i de ovenstående
ligninger. Beregningen af den invariant masse giver typisk fordelinger som
vist i Fig.~\ref{fig:spectra}.
\begin{figure}[htbp]
  \centering
  \begin{tabular}{p{.4\linewidth}p{.4\linewidth}}
    \includegraphics[width=\linewidth]{LambdaSpectrum}
    & \includegraphics[width=\linewidth]{KaonSpectrum}
  \end{tabular}
  \caption{Fordelingen til venstre er masse beregningen for
    pion-proton par; toppen svarer til $\Lambda$ og kontinuummet er
    ``baggrund'' fra \emph{tilfældige} kombinationer af pioner og protoner, der
    tilsyneladende kommer fra samme vertex (punkt) eller som er blevet
    fejlagtigt identificeret. Fordelingen til højre er massen beregnet
    for negativt og positivt ladede pioner par; toppen svarer til
    $\KZeroS$.} 
  \label{fig:spectra}
\end{figure}

\section{Signal- og baggrunds-model}
\label{sec:sigbck}

Hvis vi, for eksempel for at lede efter $\KZeroS$
V0'er, kombinerer alle $\pion-$ and $\pion+$ så finder vi kombinationer
som ikke svarer til $\KZeroS$ henfald. Disse V0'er kaldes ``baggrund''
da de ikke svarer til et ``signal''.  Modsat, V0'er der svarer til
$\KZeroS$ henfald udgør vores ``signal''.

Fordelingen af den invariante masse for baggrunds V0'er, som vist i
Fig.~\ref{fig:spectra}, er forholdsvis flad, mens den invariante masse
for signal V0'er har en skarp fordeling ovenover baggrunden.  Det
fører til en model for det invariante masse spektrum

\begin{align}
  \label{eq:sigbck}
  D(m_{\mathrm{inv}})
  &= B(m_{\mathrm{inv}}) + S(m_{\mathrm{inv}})\\
  \intertext{hvor}
  B(m_{\mathrm{inv}})
  &= a_2m_{\mathrm{inv}}^2+a_1m_{\mathrm{inv}}+a_0
  \nonumber\\
  S(m_{\mathrm{inv}})
  &= A\frac{1}{\sqrt{2\pi}\sigma}e^{-\frac{(m_\mathrm{inv}-m_0)^2}{2\sigma^2}}
    \quad, 
  \nonumber
\end{align}
det vil sige summen af baggrund ($B$) og signal ($S$).  Her modellerer
vi baggrunden med en parabel og signalet med en normal (Gauss)
fordeling. Ved at finde de parametre, der bedst beskriver den målte
fordeling - kendt som \emph{kurvetilpasning} eller \emph{fitting} - vi
kan bestemme værdien for $m_0$ det bedste estimat af hvilemasseb af
partikeltypen, og $\sigma$ det bedste estimat af partikeltypens
``bredde''. For at udtrække antallet af, for eksempel $\KZeroS$ ,
evaluerer vi integralet af hele ($D$) og baggrunden($B$) fordelingerne
og tager forskellen
\begin{align*}
N = \int_{m_1}^{m_2} \mathrm{d}m_{\mathrm{inv}}\,D(m_{\mathrm{inv}})
- \int_{m_1}^{m_2} \mathrm{d}m_{\mathrm{inv}}\,B(m_{\mathrm{inv}})
\end{align*}
hvor $[m_1,m_2]$ er det underinterval af hele intervallet for
$m_{\mathrm{inv}}$ hvor vi har tilpasset vores model $D$.  Bemærk at
vi bør have at $m_1>m_0-2\sigma$ og $m_2<m_0+2\sigma$.

Vi vil  have et robust mål for hvor tæt vores model kommer til den
målte fordeling. Så antag at for $m_i$ vi har $h_i$ tællinger med
usikkerheden $e_i=\sqrt{h_i}$, da kan vi definerer $\chi^2$-målet for
``goodness-of-fit'' som
\begin{align*}
  \chi^2 = % \frac{1}{h-l}
  \sum_{i=l}^{h}
  \frac{\left[D(m_i)-h_i\right]^2}{e_i^2}\quad,
\end{align*}
hvor $[l,h]$ definerer vores underinterval.  For en fornuftige model
og tilpasning forventer vi at
$$
\chi^2=\nu=(h-l)-N_{\mathrm{parameters}}
$$
hvor $h-l$ er antallet af observationer og
$N_{\mathrm{parameters}}=6$, så $\nu$ er antallet af frihedsgrader.
Vi kan omformulere dette til at sige at vi forventer det
\emph{reduceret} $\chi^2$, givet ved $\chi^2/\nu$, skal være tæt på
én. 

\section{Første øvelse: Visuel inspektion af begivenheder}

Øvelsen udføres i ROOT-værktøjet ved hjælp af en forenklet version af
ALICE-event displayet. For at starte værktøjet skal du køre
\begin{quote}
  \ttfamily
  root {\itshape $\langle$installation$\rangle$}/MasterClass.C
\end{quote}
hvor \texttt{{\itshape $\langle$installation$\rangle$}} er hvor Master
Class pakken blev installeret. (Instruktøren kan have installeret et
skrivebords ikon du kan bruge i stedet).

Dette vil frembringe et vindue som vist til venstre i
Fig.~\ref{fig:gui:ClassSelector}. Her skal du vælge den \emph{Strange
  Particle Production} fra \emph{Select-class\textellipsis}
(\emph{Vaelg emne\textellipsis}) vælgeren. For at starte den første
del øvelse, vælg \emph{Exercise 1: Visuel inspect p-p events} fra
\emph{Select exercise\textellipsis} (\emph{Vaelg ovelse\textellipsis})
vælgeren. Et nyt vindue som vist til højre i
Fig.~\ref{fig:gui:ClassSelector} vil dukke . I dette vindue vælger du
dit datasæt og starter begivenheds displayet med tryk på \emph{Start}.

\begin{figure}[htbp]
  \centering
  \begin{tabular}{p{.4\linewidth}p{.4\linewidth}}
    \includegraphics[width=\linewidth]{../../Base/doc/ClassSelector}
    & \includegraphics[width=\linewidth]{Part1Selector}
  \end{tabular}
  \caption{Venstre: Master Class vælger.  Vælg \emph{Strange Particle
      Production} og \emph{Exercise
      1: Visually inspect pp events}.  Højre: Datasæt
    vælger. Vælg dit datasæt og tryk \textsl{Start}}
  \label{fig:gui:ClassSelector}
\end{figure}

Det første tilgængelige datasæt er Demo-sæt. Dette datasæt indeholder
4 begivenheder, hver med en enkelt \strange{} partikel henfald i
det. Rækkefølgen er 
\begin{align*}
  \KZeroS&\rightarrow\pion++\pion-\\
  \Lambda &\rightarrow\proton+\pion-\\
  \bar{\Lambda} &\rightarrow\bar{\proton}+\pion+\\
  \Xi^-&\rightarrow\pion-+\Lambda(\rightarrow\proton+\pion-)
\end{align*}
Dette datasæt er givet for at du kan blive bekendt med henfalds
topologierne. Resten af   datasættene indeholder hver 15 pp begivenheder
ved en kollision energi på 2.76TeV, som alle indeholder \strange{}
partikel henfald (nogle begivenheder kan har mere end en). Dette er de
datasæt, du vil lave din analyse på. Instruktoren fortæller dig hvilket
datasæt du skal analyserer. Når du har valgt dit datasæt, skal du
trykke på knappen \emph{Start}. Bemærk, det kan tage lidt tid (men
ikke længere end et par minutter), før der sker noget: Værktøjet
opbygges i baggrunden. Til sidst vises begivenhedsdisplaygrænsefladen
som vist i Fig.~\ref{fig:gui:EventDisplay}. 

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\linewidth]{EventDisplay}
  \caption{Begivenheds display. Venstre side er til navigation af
    begivenhederne. Højre side er et 3D display og to 2D projektioner
    af hver begivenhed. I bunden er et værktøj til at beregne den
    invariante masse af henfaldsprodukter.}
  \label{fig:gui:EventDisplay}
\end{figure}

Kolonnen til venstre tilbyder en række muligheder: Instruktioner,
Begivenheds Navigation, visning af punkter, spor, V0'er og
kaskader. Derudover er der under ``Encyclopedia'' en kort beskrivelse
af ALICE og dets hovedkomponenter og eksempler på decay
topologier. Nederst i bunden er knapper til at udskrive resultaterne
til en PDF, eksportere resultaterne til en fil og forlade værktøjet.

Hændelsesdisplayet viser tre visninger af ALICE (3-dimensionel
visning, $rp$-projektion og $rz$-projektion).Du kan vælge de
oplysninger, der vises for hver begivenhed. Hvis du klikker på den
relevante visningsindstilling, ser du alle punkter og spor i
begivenheden; Hvis du klikker på V0'er (og kaskader)
visningsindstillingerne, vil V0'er (og kaskader) fremhæves, hvis de
eksisterer. Når et V0 er fundet, kan resten af   spor og punkter af
begivenheden kan fjernes fra displayet, så kun sporene i forbindelse
med V0'et er vist. Farvekonventionen er at positivt ladet spor fra V0
er \emph{røde}, negativt ladede sporer \emph{grønne} (og "bachelorer",
i tilfælde af kaskader, \emph{lilla}).

Ved at klikke på hvert spor vises værdierne for impuls komponenterne
og partikelmassen i den relevante række i bundværktøjet. Når du
klikker på flere partikler fra samme henfald, vil værktøjet automatisk
beregne den invariant masse for dig. Når du har valgt alle
datterpartikler, skal du vælge mellem partikel type
($\mathrm{K}^{\mathrm{S}}_{0}$, $\Lambda$, $\bar{\Lambda}$, or $\Xi$)
i \emph{Vælg partikel type}. Når du er glad ved dit valg, trykker du
\emph{Indsend} for at registrere observationen.

Når du har fundet alle \strange{} henfald i en begivenhed,
skal du trykke på \emph{Begivenheden analyseret!} knappen til venstre.

Når du sender flere og flere observationer af partikler, samler vi
statistikker over den invariante massespektrum for de fire partikler
($\mathrm{K}^{\mathrm{S}}_{0}$, $\Lambda$, $\bar{\Lambda}$, and $\Xi$)
vi undersøger. For at se disse spektre, vælg fanen \emph{Begivenheds
  Karakteristik} øverst. Bemærk, da vi bruger dine fantastiske
mønstergenkendelsesfærdigheder som et menneske (i modsætning til en
computer) har vi ingen ``baggrund'' som vist i figur 3 . I stedet har
vi de invariant massetoppe, der er centreret omkring de kendte masser.

\subsection{Øvelsen --- Analyser begivenheder og find de mærkelige
  hadroner}

Analysedelen består af identifikation og tælling af \strange{}
partikler i et givent datasaet, der typisk indeholder 15
begivenheder. Når du starter øvelsen, skal du gå til elevtilstand og
vælg det datasaet du vil analysere. I øjeblikket er der 20
forskellige datasæt fra proton-proton kollisioner ved 
$\sqrt{s}=7\mathrm{TeV}$ (\emph{center-of-mass energy}).

Når man kigger på hver begivenhedsvisning, skal man i første omgang
klikke på \emph{Spor} og \emph{Punkter}; du kan observere
kompleksiteten af   begivenhederne og det høje antal spor, der
produceres af kollisionerne inde i detektorerne. De fleste af disse
spor er pioner.

Ved at fravælge \emph{Spor} og \emph{Klynger} og vælge \emph{V0'er} og
\emph{Kasakder}, fremhæves alle spor fra V0 og kaskade henfald. Fra V0
topologien kan du prøve at gætte hvilken type moder partikelen er. Ved
at klikke på hvert spor får du oplysningerne - ladning, de
trekomponenter af impuls vektoren, og massen af   den mest sandsynlige
partikel associeret med det spor. Dette er fundet ud fra informationen
fra de forskellige detektorer anvendt til partikelidentifikation. Fra
henfaldsprodukterne kan man allerede gætte, hvad moderpartiklen
er. For at bekræfte det beregner du den invariant masse som beskrevet
i afsnit~\ref{sec:invmass} og sammenlign dens værdi med 
værdierne i tabellen af din regnemaskine eller dem, der er angivet i 
Tab.~\ref{tab:masses}

\begin{table}[htbp]
  \centering
  \begin{tabular}{|cccc|}
    \hline
    \multicolumn{2}{|c}{\textbf{Invariante masse}}
    & \textbf{Negative datter}
    & \textbf{Moder type}\\
    \hline
    $497\pm13$MeV
    & $[484,510]$MeV
    & -
    & $\mathrm{K}^{0}_{\mathrm{S}}$ \\
    \hline
    $1115\pm5$MeV
    & $[1110,1120]$MeV
    & $\pi^{-}$
    & $\Lambda$\\
    &
    & $\bar{\mathrm{p}}$
    & $\bar{\Lambda}$\\
    \hline
    $1321\pm10$MeV
    & $[1311,1331]$MeV
    & -
    & $\Xi^{-}$
    \\
    \hline
  \end{tabular}
  \caption{Identifikations kriterier for \strange{} partikel henfald}
  \label{tab:masses}
\end{table}

Afhængigt af resultatet vælger du den relevante partikel i
partikelvælgeren og trykker \emph{Indsend} knappen. På denne måde
tilføjes denne observation i det tilsvarende invariant masse
histogram. Det kan ske, at den beregnede masse ikke svarer til nogen
af   ovennævnte værdier; dette er ``baggrund'': sporene vises som kommer
fra et sekundært vertex, men i dette tilfælde er vertexet fejlagtigt
identificeret. I forbindelse med denne øvelse vil vi ignorere disse
V0'er - men tjek at du ikke har overset en kombination ser virker
fornuftig.

\subsection{Præsentation af resultaterne}


Hvis du trykker på knappen \emph{Print\textellipsis} til venstre, får
du en PDF med de invariant massespektre i. Hvis du trykker på knappen
\emph{Eksporter\textellipsis} kan lave en ROOT-fil med histogrammerne
i den. Dette kan sendes til instruktøren (via e-mail, netværksoverførsel,
USB-stick eller andent), så hun kan indsamler resultaterne fra alle
datasæt i et endeligt resultat.

\subsection{Indsamling af resultater}

Når et datasæt er blevet analyseret, skal den tilsvarende ROOT-fil
sendes til instruktøren. Hun kan derefter bruge indstillingen
\emph{Instruktør Øvelse 1: Saml resultater} i den første grænseflade
(se Fig.~\ref{fig:gui:ClassSelector}) som vil oprette en særlig
grænseflade til dette formål. Instruktøren viser så de endelige
resultater til hele klassen, og du bør diskutere disse i plenum.
 
\section{Anden øvelse: Strangeness enhancement}

Begivnhedsdisplayet er et kraftfuldt værktøj, der hjælper med at
kontrollere kvaliteten af   data og giver en fornemmelse af hvordan en
begivenhed ser ud. Men i den virkelige verden udføres data analyse ikke
visuelt -det ville være alt for besværligt og tidskrævende. For at
analysere de millioner af begivenheder vi indsamler dagligt på LHC
kører vi programmer for at kigge efter V0'er i større datasæt
og opbygger invariante massespektre (som vist i
Fig.~\ref{fig:spectra}) og ekstraher antallet af \strange{}
partikler.  I denne øvelse vil vi udtrække antallet af \strange{}
partikler fra invariante massespectra.

Når vi har fundet antallet af $\KZeroS$, $\Lambda$, og $\bar\Lambda$
kan vi beregne udbyttet (\emph{yield}, antallet af partikler per
begivenhed) for hver partikeltype. \emph{Yield} for partikeltype $i$
er givet ved
\begin{align}
Y_i = \frac{N_i}{N_{\mathrm{events}}\varepsilon_{i}}\label{eq:yield}
\end{align}
hvor $N_i$ er antallet af partikler af typen $i$ der er observeret,
$N_{\mathrm{events}}$ er antallet af begivenheder, og $\varepsilon_i$
er \emph{effektiviteten} for at detekterer V0'er af typen $i$.  Vi vil
dog bestemme udbyttet for forskellige valg af \emph{centralitet}.

\subsection{Centralitet}



Når vi studerer tunge-ion kollisioner klassificerer vi typisk
begivenhederne i forskellige klasser afhængig af kollisionens
\emph{centralitet}.

Bly kernen er meget større end protonen, og dette fører til forskelle
i hvordan partiklerne kollidere. I tilfælde af $\proton-\proton$ er
der kun \'en slags kollision, mens bly kollisioner er differentieret
af størrelsen af regionen af hver kerne, der overlapper hinanden.

For at se forskellen mellem $\proton-\proton$ og bly-bly kollisioner
definerer vi først forskellige begivenhedsklasser for tung-ions
kollisioner. Et anvendte kriterier er kollisions centraliteten, som er
relateret til stødparameteren (\emph{impact parameter}, afstanden
mellem de kolliderende kerner vinkelret på kollisionsaksen). Men denne
parameter er ikke direkte målelig. En af de måder, hvorpå ALICE
bestemmer centraliteten af   begivenhederne er fra signalamplituden i
V0\footnote{Her er V0 en detektor i ALICE, og må ikke forvirres med
  V0'er fra henfald af partikler.} tællere (2 tællerer af plastik
scintillatorer placeret $+330\mathrm{cm}$ og $-90\mathrm{cm}$ fra
interaktionspunktet). Dette er vist i højre side af
Fig.~\ref{fig:cent}. Et kvantitativt estimat af kollisions
centraliteten er givet ved antallet af deltagende nukleoner
($N_{\mathrm{part}}$) som illustreret med rødt på venstre side af
Fig.~\ref{fig:cent}.

\begin{figure}[htbp]
  \centering
  \begin{tabular}[t]{p{.36\linewidth}p{.53\linewidth}}
    \vbox{}
    \includegraphics[width=\linewidth]{Npart}
    & \vbox{}
      \includegraphics[width=\linewidth]{Centrality}
  \end{tabular}
  \caption{Venstre: Illustration af antal deltagere
    $N_{\mathrm{part}}$  for en kollision med ikke-nul
    stødparameter $b$. Højre: Klassificering af begivenheder i
    henhold til signalet i V0 detektoren. Bemærk, en
    lille centralitets klasse svarer til stort nukleare overlap, mens
    store centralitets klasser svarer til små overlapning.}
  \label{fig:cent}
\end{figure}

I denne del af øvelsen analyserer du datasæt tilhørende forskellige
centralitets klasser, og vil se efter V0'er og beregner antallet af
$\KZeroS$, $\Lambda$ og $\bar\Lambda$ og bestemme \emph{yeild} af hver
af disse i hver centralitets klasse. 

\subsection{Øvelsen: Tilpas model til invariant massespektra}

For at starte øvelsen, kør
\begin{quote}
  \ttfamily
  root {\itshape $\langle$installation$\rangle$}/MasterClass.C
\end{quote}
hvor \texttt{{\itshape $\langle$installation$\rangle$}} er hvor Master
Class pakken er installeret (instruktoren har muligvis lavet et
skrivebordsikon du kan bruge i stedet). 

Dette vil frembringe et vindue som vist til venstre i
Fig.~\ref{fig:gui:ClassSelector2}. Her skal du vælge \emph{Strange
  particle production} fra \emph{Select-class\textellipsis}
vælgeren. For at starte den anden øvelse skal du vælge \emph{Ovelse 2: 
Find top over baggrunden} fra \emph{Select exercise\textellipsis}
vælgeren. Du vil nu se tilpasnings værktøjet som vist
til højre i Fig.~\ref{fig:gui:ClassSelector2}.

\begin{figure}[htbp]
  \centering
  \begin{tabular}{p{.295\linewidth}p{.67\linewidth}}
    \includegraphics[width=\linewidth]{../../Base/doc/ClassSelector}
    & \includegraphics[width=\linewidth]{SpectraFit}
  \end{tabular}
  \caption{Venstre: Master Class vaelger. Vælg \emph{Strange Particle
      Production} og \emph{Ovelse 2: tilpas top over baggrunden}. Højre:
    Værktøj til analyse af invariant massespektra.}
  \label{fig:gui:ClassSelector2}
\end{figure}

Til venstre for spektra tilpasnings værktøj har du et træ af spektra
du skal tilpasse vores model til. Klik på et spektrum for at vise det
på den højre side. Prøv derefter at tilpasse vores signal-baggrunds
model til spektrene ved at trykke på \emph{Tilpas} i bunden. Du kan
begrænse placeringen af   toppen ved hjælp af skyderen \emph{Graenser pa
  signal} nederst. Du kan også begrænse intervallet af   hele
tilpasningen ved hjælp af skyderen \emph{Tilpasningsinterval}. Efter
at have udført en tilpasning, vises den tilpasset model med fundne
parametre som en linje på spektrene. Samtidig vises også i det
reduceret $\chi^2$ ($\chi^2/\nu$). Dette tal skal være tæt på \'en
(1).


Når du er tilfreds med tilpasningen, kan du trykke på \emph{Accepter}
knappen og gå videre til de næste spektre. Ved at trykke på \emph{Accepter}
knappen markeres spektrene som færdige, og de tilpasset parametre
gemmes og antallet af partikler som beskrevet beregnes (se
afsnit~\ref{sec:sigbck}) og gemmes. Du kan se disse ved at vælge fanen
\emph{Opsummering} i toppen.  Den fundne invariante masse $m_0$ og
bredden $\sigma$, begge med usikkerheder, lagres ligeledes. Disse kan
du se i fanen \emph{Masse og bredde}.

Bemærk, du vil ikke se det
faktiske antal \strange{} partikler, der er fundet for et givent 
spektrum, indtil du trykker \emph{Accepter} knappen. Dette skyldes, at
$\chi^2/\nu$ er alt hvad du behøver for at afgøre om tilpasningen er
god eller ej. Når du har accepteret tilpasningen, får du de
ekstraherede værdier. Dette er beslægtet med hvad vi kalder
\emph{Blind Analysis}.

Du bør udfører så mange tilpasninger som muligt. Instruktøren kan give
dig et sæt prioriteter der skal gennemføres først. Når du er færdig
med de tilpasningner, skal du trykke på knappen
\emph{Eksporter\textellipsis} for at gemme resultaterne i en ROOT-fil.

Efter at vi har bestemt antallet af $\KZeroS$, $\Lambda$, og
$\bar{\Lambda}$ i hvert centralitets område, kan vi beregne
\emph{yield} af hver partikeltype i hvert centralitetsområde. Til
dette har vi brug for et par tal angivet i Tab.~\ref{tab:numbers}. Endelig kan
vi beregne forholdet mellem \strange{} partikler i tung-ion kollisioner
til det samme i proton-proton kollisioner som

\begin{align}
  \label{eq:enhance}
  H_i &=
        \frac{Y^{\mathrm{Pb-Pb}}_i}{N^{\mathrm{Pb-Pb}}_{\mathrm{part}}}
        \frac{N^{\mathrm{pp}}}{Y^{\mathrm{pp}}_i}\quad,
\end{align}
hvor $Y_i$, beregnet ifølge \eqref{eq:yield} centralitetsinterval
i tung-ion kollisioner og en gang for $\proton-\proton$ kollisioner. 

Hvis dette tal er større end 1, så har vi \enhancement{} --- det vil
sige, at det er ``lettere'' at lave \strange{} kvarker i det varme og
tætte medium skabt i tung-ion kollisioner end i proton-proton
kollisioner.

\begin{table}[htbp]
  \centering
  \begin{tabular}{|c|rr|rrr|}
    \hline
    \textbf{Centrality}
    & $\mathbf{N}_{\mathbf{part}}$
    & $\mathbf{N}_{\mathbf{events}}$
    & $\varepsilon_{\mathrm{K}^0_{\mathrm{S}}}$
    & $\varepsilon_{\Lambda}$
    & $\varepsilon_{\bar{\Lambda}}$
    \\[1ex]
    \hline
    pp
    & $2$
    & $5260$
    & $0.26$
    & $0.67$
    & $0.44$
    \\
    $\phantom{8}0-10$
    & $360$
    & $213$
    & $0.26$
    & $0.20$
    & $0.20$
    \\
    $10-20$
    & $260$
    & $290$
    & $0.26$
    & $0.21$
    & $0.21$
    \\
    $20-30$
    & $186$
    & $302$
    & $0.29$
    & $0.22$
    & $0.22$
    \\
    $30-40$
    & $129$
    & $310$
    & $0.29$
    & $0.22$
    & $0.22$
    \\
    $40-50$
    & $85$
    & $302$
    & $0.29$
    & $0.22$
    & $0.22$
    \\
    $50-60$
    & $52$
    & $300$
    & $0.29$
    & $0.20$
    & $0.20$
    \\ 
    $60-70$
    & $30$
    & $315$
    & $0.35$
    & $0.20$
    & $0.20$
    \\ 
    $70-80$
    & $16$
    & $350$
    & $0.26$
    & $0.20$
    & $0.20$
    \\
    \hline
  \end{tabular}
  \caption{Forskellige tal}
  \label{tab:numbers}
\end{table}
\end{document}
%% Local Variables:
%% ispell-dictionary: "dansk"
%% End:

% LocalWords:  observable gluoner nukleoner normaliseres anti-kvarker
% LocalWords:  kvark-gluon ned-kvarkene
