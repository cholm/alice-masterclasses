\documentclass[11pt,twoside]{article}

\usepackage[margin=2cm,a4paper]{geometry}
\usepackage{graphicx}
\usepackage{amstext}
\usepackage{amsmath}
\usepackage{hyperref}
% \def\includegraphics[#1]#2{}

\title{Finding strange particles}
\author{ALICE Collaboration}
\date{\today}
\setlength{\parindent}{0pt}
\setlength{\parskip}{1ex}

\begin{document}
\thispagestyle{empty}
\newdimen\tempdima
\tempdima=\textwidth
\advance\tempdima-4.5cm
{\makeatletter\xdef\ddate{\@date}}
\begin{tabular}[t]{@{}p{2cm}@{}p{\tempdima}@{}p{2cm}@{}}
  \multicolumn{3}{c}{\Large EUROPEAN ORGANIZATION FOR NUCLEAR RESEARCH}\\[8mm]
  \includegraphics[height=2cm]{../../Base/doc/ALICE_logo}
  &\hfill
  &\includegraphics[height=2cm]{../../Base/doc/CERN_logo}\\[3mm]
  &
  \multicolumn{2}{r}{\ddate}
\end{tabular}
\begin{center}
  {\makeatletter\Large\textbf{\@title}}
  \par
  \vspace*{5mm}
  \begin{tabular}[t]{c}
    {\makeatletter\large\@author}
  \end{tabular}
\end{center}
{\let\thempfn\empty
  \footnotetext{\hspace*{-1.5\parindent}\noindent\normalsize%
    \copyright\,\the\year\ CERN for the benefit of the ALICE
    Collaboration.\newline%
    Reproduction of this article or parts of it is allowed as specified
    in the \href{http://creativecommons.org/licenses/by/4.0}{CC-BY-4.0
      license}.}}
\tableofcontents

\section{Overview}
This master class consists of a search for strange particles, produced
from collisions at LHC and recorded by the ALICE. It is
based on the recognition of their decay patterns known as V0-decays,
such as %
$\mathrm{K}^{0}_{\mathrm{S}}\rightarrow\pi^{+}+\pi^{-}$, %
$\Lambda\rightarrow \mathrm{p} + \pi^{-}$, %
and cascades, such as %
$\Xi^{-}\rightarrow \Lambda + \pi^{-}$ %
where the $\Lambda$ further decays to a proton and a negatively
charged pion ($\Lambda \rightarrow\mathrm{p} +\pi^{-}$), so that we
have the final decay products $\mathrm{p}+\pi^{-}+\pi^{-}$.%
The identification of the strange particles is based on the topology
of their decay combined with the identification of the decay products;
the information from the tracks is used to calculate the invariant
mass of the decaying particle, as an additional confirmation of the
decaying particle species.

In what follows the ALICE and its physics goals are first
presented briefly, then the physics motivation for this analysis. The
method used for the identification of strange particles as well as the
tools are described in detail; then all the steps of the exercise are
explained followed by the presentation of the results as well as the
method of collecting and merging all results. In the end the large
scale analysis is presented.

\section{Introduction}

ALICE (A Large Ion Collider Experiment), one of the four large
experiments at the CERN Large Hadron Collider, has been designed to
study heavy ion collisions. It also studies proton-proton collisions,
which primarily provide reference data for the heavy ion
collisions. In addition, the proton-proton collision data allow for a
number of genuine proton proton physics studies.  The ALICE detector
has been designed to cope with the highest particle multiplicities
anticipated for collisions of lead nuclei at the extreme energies of
the LHC.

\section{The ALICE Physics}

Quarks are bound together into protons and neutrons by a force known
as the strong interaction, mediated by the exchange of force carrier
particles called gluons. The strong interaction is also responsible
for binding together the protons and neutrons inside atomic nuclei.

Even though we know that quarks are elementary particles that build up
all known hadrons, no quark has ever been observed in isolation: the
quarks, as well as the gluons, seem to be bound permanently together
and confined inside composite particles, such as protons and
neutrons. This is known as confinement. The exact 
mechanism that causes it remains unknown.

Although much of the physics of strong interaction is, today, well
understood, two very basic issues remain unresolved: the origin of
confinement and the mechanism of the generation of mass. Both are
thought to arise from the way the properties of the vacuum are
modified by strong interaction.

The current theory of the strong interaction (called Quantum
Chromo-Dynamics) predicts that at very high temperatures and/or very
high densities, quarks and gluons should no longer be confined inside
composite particles. Instead they should exist freely in a new state
of matter known as quark-gluon plasma.

Such a transition should occur when the temperature exceeds a critical
value estimated to be about $100\,000$ times hotter than the core of
the Sun! Such temperatures have not existed in Nature since the birth
of the Universe. We believe that for a few millionths of a second
after the Big Bang the temperature was indeed above the critical
value, and the entire Universe was in a quark-gluon plasma state.

When two heavy nuclei approach each other at a speed close to that of
light and collide these extreme conditions of temperature are
recreated and release the quarks and the gluons.  Quarks and gluons
collide with each other creating a thermally equilibrated environment:
the quark–gluon plasma. The plasma expands and cools down to the
temperature ($10^{12}{\phantom{|}}^{\circ}\mathrm{C}$) at which quarks and gluons
regroup to form ordinary matter, barely $10^{-23}$ seconds after the
start of the collision.  ALICE will study the formation and the
properties of this new state of matter.

\section{Strangeness enhancement as a %
  signature for quark gluon plasma} 

The diagnosis and the study of the properties of quark-gluon plasma
(QGP) can be undertaken using quarks not present in matter seen around
us. One of the experimental signatures relies on the idea of
strangeness enhancement. This was one of the first observable of
quark-gluon plasma, proposed in 1980. Unlike the up and down quarks,
strange quarks are not brought into the reaction by the colliding
nuclei. Therefore, any strange quarks or antiquarks observed in
experiments have been ``freshly'' made from the kinetic energy of
colliding nuclei. Conveniently, the mass of strange quarks and
antiquarks is equivalent to the temperature or energy at which
protons, neutrons and other hadrons dissolve into quarks. This means
that the abundance of strange quarks is sensitive to the conditions,
structure and dynamics of the deconfined matter phase, and if their
number is large it can be assumed that deconfinement conditions were
reached.

In practice, the strangeness enhancement can be observed by counting
the number of strange particles, that is particles containing at least
one strange quark, and calculating the ratio of strange to non-strange
particles. If this ratio is higher than that given by the theoretical
models that do not foresee the creation of QGP, then enhancement has
been observed.

Alternatively, for lead ion collisions, the number of strange
particles is normalised to the number of nucleons participating in the
interaction and compared with the same ratio for proton collisions.

\section{Strange particles}
Strange particles are hadrons containing at least one strange
quark. This is characterized by the quantum number of
``strangeness''. The lightest neutral strange meson is the
$\mathrm{K}^{0}_{\mathrm{S}}$ ($\mathrm{d}\bar{\mathrm{s}}$) and the
lightest neutral strange baryon is the $\Lambda$ (uds), characterized
as hyperon.

Here we will be studying their decays, for example
$\mathrm{K}^{0}_{\mathrm{S}}\rightarrow \pi^{+}+\pi^{-}$, %
$\Lambda\rightarrow \mathrm{p} + \pi^{-}$.  %
In these decays the quantum number of strangeness is not conserved,
since the decay products are only composed of up and down quarks.
Therefore these are not strong decays (which in addition would be very
fast, with a $\tau = 10^{-23}\mathrm{s}$) but weak decays, in which
the strangeness can be conserved ($\Delta S=0$) or change by 1
($\Delta S=1$). For these decays the mean life $\tau$ is between
$10^{-8}\mathrm{s}$ and $10^{-10}\mathrm{s}$.  For particles
travelling close to the speed of light, this means that the particle
decays at a distance (on average) of some centi-metre from the point
of production (e.g. from the point of the interaction).

\section{How we look for strange particles}

The aim of the class is to search for strange particles produced from
collisions at LHC and recorded by the ALICE.

As mentioned in the previous section, strange particles do not live
long; they decay soon after their production.  However, they live long
enough to travel some cm distance from the interaction point (IP),
where they were produced (primary vertex).  Their search is thus based
on the identification of their decay products, which must originate
from a common secondary vertex.

Neutral strange particles, such as $\mathrm{K}^{0}_{\mathrm{S}}$ and
$\Lambda$, decay giving a characteristic decay pattern, called V0. The
mother particle disappears some centi-metre from the interaction point
and two oppositely charged particles appear in its place, which are
bent in opposite directions inside the magnetic field of the ALICE
solenoid.

The decays we will be looking for are shown in Fig.~\ref{fig:v0_patterns}
\begin{figure}[htbp]
  \centering
  \begin{tabular}{p{.45\linewidth}p{.195\linewidth}p{.28\linewidth}}
    \includegraphics[width=\linewidth]{../../Base/doc/kaon}
    & \includegraphics[width=\linewidth]{../../Base/doc/lambda}
    & \includegraphics[width=\linewidth]{../../Base/doc/antilambda}
    \\
    $\mathrm{K}^{0}_{\mathrm{S}}\rightarrow\pi^++\pi^-$
    & $\Lambda\rightarrow\mathrm{p}+\pi^-$
    & $\bar{\Lambda}\rightarrow\bar{\mathrm{p}}+\pi^+$
  \end{tabular}
  \caption{Decay patterns of $\mathrm{K}^S_0$, $\Lambda$, and
    $\bar{\Lambda}$.  Red tracks are positively charged particles, and
    green tracks are negatively charged particles.}
  \label{fig:v0_patterns}
\end{figure}

We see that for a $\pi+\pi$ final state the decay pattern is
quasi-symmetric whereas in the $\pi+\mathrm{p}$ final state the radius
of curvature of the proton is bigger than that of the $\pi$: due to its
higher mass the proton carries most of the initial momentum.

We will also be looking for cascade decays of charged strange
particles, such as the $\Xi^{-}$ this decays into $\pi^{-}$ and
$\Lambda$; the $\Lambda$ then decays into $\pi^-$ and p; the initial
$\pi$ is characterized as a bachelor (single charged track) and is
shown in purple.  The topology is shown in
Fig.~\ref{fig:cascade_pattern}.
\begin{figure}[htbp]
  \centering
  \begin{tabular}{p{.45\linewidth}}
    \includegraphics[width=\linewidth]{../../Base/doc/xi}
    \\
    $\Xi^-\rightarrow\pi^{-}+\Lambda(\rightarrow\mathrm{p}+\pi^-)$
  \end{tabular}
  \caption{Decay pattern of a $\Xi^-$. Charged positive and negative
    tracks are red and green, respectively, while the bachelor track
    is purple}.
  \label{fig:cascade_pattern}
\end{figure}

The search for V0s is based on the decay topology and the
identification of the decay products; an additional confirmation of
the particle identity is the calculation of its mass; this is done
based on the information (mass and momentum) of the decay products as
described in the following section.

\section{The (invariant) mass calculation}
\label{sec:invmass}
We consider the decay of a neutral kaon in to two charged pions
$$
\mathrm{K}^{0}_{\mathrm{S}}\rightarrow\pi^++\pi^-
$$
Let $E$, $\mathbf{p}=(p_x,p_y,p_z)$, and $m$ be the total energy,
momentum (a vector), and mass, respectively, of the mother particle
($\mathrm{K}^{0}_{\mathrm{S}}$).  Similarly, let $E_1$,
$\mathbf{p}_1=(p_{1,x},p_{1,y},p_{1,z})$, and $m_1$ be the total
energy, momentum, and mass, respectively, of the first decay product
($\pi^+$), and $E_2$, $\mathbf{p}_2=(p_{2,x},p_{2,y},p_{2,z})$, and
$m_2$ be the total energy, momentum, and mass, respectively, of the
second decay product ($\pi^-$).  From conservation of energy and
momentum, we have
\begin{align}
  E & = E_1+E_2\\
  \mathbf{p} &= \mathbf{p}_1+\mathbf{p}_2\quad,
\end{align}
respectively. From specific relativity (setting the speed-of-light
$c=1$), we have that
\begin{align*}
  E^2
  &= p^2 + m^2
  & E_1^2
  &=p_1^2+m_1^2
  & E_2^2
  &=p_2^2+m_2^2\\
  p&=\sqrt{p_x^2+p_y^2+p_z^2}
  & p_1
  &=\sqrt{p_{1,x}^2+p_{1,y}^2+p_{1,z}^2}
  & p_2
  &=\sqrt{p_{2,x}^2+p_{2,y}^2+p_{2,z}^2}
\end{align*}
where $p, p_1, p_2$ are the lengths of the momentum vectors.  From
this we find that
\begin{align*}
  m^2 &= E^2 - p^2 = (E_1+E_2)^2 - (\mathbf{p}_1+\mathbf{p}_2)^2 \\
      &= E_1^2+E_2^2 +2E_1E_2
        - \mathbf{p}_1\cdot\mathbf{p}_1
        - \mathbf{p}_2\cdot\mathbf{p}_2
        - 2\mathbf{p}_1\cdot\mathbf{p}_2\\
  \intertext{Since for any vector $\mathbf{v}\cdot\mathbf{v}=v$, we have}
      &= E_1^2+E_2^2+2E_1E_2 - p_1^2-p_2^2 -
        2\mathbf{p}_1\cdot\mathbf{p}_2\\
  \intertext{and since for 3-dimensional vectors
  $\mathbf{v}\cdot\mathbf{u}=v_xu_x+v_yu_y+v_zu_z$, we have} 
      &= E_1^2+E_2^2+2E_1E_2 - p_1^2-p_2^2 -
        2\left(p_{1,x}p_{2,x}+p_{1,y}p_{2,y}+p_{1,z}p_{2,z}\right)
\end{align*}
We can therefore calculate the mass of the initial particle from the
masses and the momenta components of the daughter particles. The
masses of the daughter particles $m_1$ and $m_2$ are known: a number
of different detectors in ALICE identify particles.

The momenta of the daughter particles $\mathbf{p}_1$ and
$\mathbf{p}_2$ are found by measuring the radius of curvature of their
trajectory due to the known magnetic field.  In the exercise we use
the three components of the momentum vector of each track associated
with the V0 decay, as in the above equations.
 
The invariant mass calculation gives typically distributions as shown
in Fig.~\ref{fig:spectra}.
\begin{figure}[htbp]
  \centering
  \begin{tabular}{p{.4\linewidth}p{.4\linewidth}}
    \includegraphics[width=\linewidth]{LambdaSpectrum}
    & \includegraphics[width=\linewidth]{KaonSpectrum}
  \end{tabular}
  \caption{The distribution on the left is the mass calculated for
    pion-proton pairs; the peak corresponds to $\Lambda$ and the
    continuum is ``background'' from random combinations of pions and
    protons which appear as coming from the same secondary vertex or
    that have been misidentified; the distribution on the right is the
    mass calculated for negatively and positively charged pion pairs;
    the peak corresponds to $\mathrm{K}^{0}_{\mathrm{S}}$.}
  \label{fig:spectra}
\end{figure}

\section{Signal and background model}
\label{sec:sigbck}

If, while looking for $\mathrm{K}^0_{\mathrm{S}}$ V0's, we combine all
all $\pi^-$ and $\pi^+$, we may get combinations that does not
correspond to $\mathrm{K}^0_{\mathrm{S}}$ decays.  Such V0's are
called ``background'' since they do not correspond to a ``signal''.
On the other hand, V0's that do correspond to a
$\mathrm{K}^0_{\mathrm{S}}$ decay constitute our ``signal''.

As shown in Fig.~\ref{fig:spectra}, the distribution of background
V0 invariant masses is relatively flat, while the invariant mass of
the signal V0's is more sharply distributed and is on top of the
background distribution.  This leads to a model
for the invariant mass spectra
\begin{align}
  \label{eq:sigbck}
  D(m_{\mathrm{inv}})
  &= B(m_{\mathrm{inv}}) + S(m_{\mathrm{inv}})\\
  \intertext{where}
  B(m_{\mathrm{inv}})
  &= a_2m_{\mathrm{inv}}^2+a_1m_{\mathrm{inv}}+a_0
  \nonumber\\
  S(m_{\mathrm{inv}})
  &= A\frac{1}{\sqrt{2\pi}\sigma}e^{-\frac{(m_\mathrm{inv}-m_0)^2}{2\sigma^2}}
    \quad, 
  \nonumber
\end{align}
that is, the sum of a background ($B$) and signal ($S$) component.
Here, we model the background with a parabola and the signal by a
Gaussian distribution.  By finding the parameters that best describe
the measured distribution --- known as fitting --- we can determine
the value for $m_0$, the best estimate of the rest mass of the
particle specie, and $\sigma$, the best estimate of the width of the
particle specie. To extract the number of, for example
$\mathrm{K}^0_{\mathrm{S}}$, we can evaluate the integral of the full
($D$) and the background ($B$) distributions, and take the difference
\begin{align*}
N = \int_{m_1}^{m_2} \mathrm{d}m_{\mathrm{inv}}\,D(m_{\mathrm{inv}})
- \int_{m_1}^{m_2} \mathrm{d}m_{\mathrm{inv}}\,B(m_{\mathrm{inv}})
\end{align*}
where $m_1$ and $m_2$ is some sub-range of the range in
$m_{\mathrm{inv}}$ where we fitted in.  Note, that the we should have
$m_1>m_0-2\sigma$ and $m_2<m_0+2\sigma$.

Note, we must have some robust measure of how close our model comes to
the measured distribution.  So assume that for $m_i$ we have $h_i$
counts, with an uncertainty of $e_i=\sqrt{h_i}$ then we define the
$\chi^2$ measure of goodness-of-fit to be 
\begin{align*}
  \chi^2 = \frac{1}{h-l}\sum_{i=l}^{h}
  \frac{\left[D(m_i)-h_i\right]^2}{e_i^2}\quad,
\end{align*}
where $[l,h]$ defines our fit range. For a good fit we expect
$$
\chi^2=\nu=(h-l)-N_{\mathrm{parameters}}
$$
where $h-l$ is the number of observations and
$N_{\mathrm{parameters}}=6$, so that $\nu$ is the number degrees of
freedom.  We can reformulate this to say that we expect the reduced
$\chi^2$, given as $\chi^2/\nu$ should be close to unity (1).


\section{First exercise: Visual inspection of events}

The exercise is done in the ROOT framework, using a simplified version
of the ALICE event display.  To start the tool, run
\begin{quote}
  \ttfamily
  root {\itshape $\langle$installation$\rangle$}/MasterClass.C
\end{quote}
where \texttt{{\itshape $\langle$installation$\rangle$}} is where the
master class package was installed.  (The instructor may have
installed a Desktop launcher you can use instead).

This will bring up a window as shown on the left in
Fig.~\ref{fig:gui:ClassSelector}.  Here, you should choose the
\emph{Strange Particle Production} from the \emph{Select class...}
selector.  Then, to start the first part exercise, select \emph{Part
  1: Visually inspect pp events} from the \emph{Select exercise...}
selector.  This will pop up a new window as shown on the right in
Fig.~\ref{fig:gui:ClassSelector}.  In this window you select your data
set and launch the event display by pressing \textsl{Start} 

\begin{figure}[htbp]
  \centering
  \begin{tabular}{p{.4\linewidth}p{.4\linewidth}}
    \includegraphics[width=\linewidth]{../../Base/doc/ClassSelector}
    & \includegraphics[width=\linewidth]{Part1Selector}
  \end{tabular}
  \caption{Left: Master Class selector.  Chose \emph{Strange Particle
      Production} and \emph{Exercise
      1: Visually inspect pp events}.  Right: Data-set
    selector. Select your data set and press \textsl{Start}}
  \label{fig:gui:ClassSelector}
\end{figure}

The first available data set is \textit{Demo set}.  This data set
contains 4 events, each with a single strange-particle decay in
it. The order is
\begin{align*}
  \mathrm{K}^{0}_{\mathrm{S}}&\rightarrow\pi^++\pi^-\\
  \Lambda &\rightarrow\mathrm{p}+\pi^-\\
  \bar{\Lambda} &\rightarrow\bar{\mathrm{p}}+\pi^+\\
  \Xi^-&\rightarrow\pi^{-}+\Lambda(\rightarrow\mathrm{p}+\pi^-)
\end{align*}
This data set is provided for you to get acquainted with the decay
topologies. The rest of the data sets contain 15 pp events at 2.76TeV,
which all contain strange-particle decays (some events may have more
than one).  These are the data sets you will do your analysis on.  The
class instructor will tell you which data set to analyze.

Once you have selected your data set, hit the \textsl{Start} button.
Note, it may take some time (but no longer than a couple of minutes)
before  something happens: The tool is being build in the background.
Eventually, the event display interface will show up, as shown in
Fig.~\ref{fig:gui:EventDisplay}. 

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\linewidth]{EventDisplay}
  \caption{The event display. Left-hand side shows the event
    navigation interface.  Right-hand side is the 3D event display,
    and two 2D projections.  At the bottom is the tool to calculate
    the invariant mass of decay products.}
  \label{fig:gui:EventDisplay}
\end{figure}

The column on the left offers a number of options: Instructions, Event
Navigation, display of clusters, tracks, V0s and cascades. In
addition, under ``Encyclopedia'', there is a brief description of the
ALICE detector and its main components, and examples of the decay
topologies.  At the very bottom are buttons to print the results to a
PDF, export the results to a file, and to exit the tool.

The event display shows three views of ALICE (3-dimensional view,
$r\varphi$ projection and $rz$ projection). You can select the
information displayed for each event. If you click on the relevant
display option, you see all the clusters and tracks of the event; if
you click the V0 (and cascade) display options, the V0s (and cascades)
are highlighted, if they exist.  Once a V0 is found, the rest of
tracks and clusters of the event can be removed from the display so
that only the tracks associated with the V0 are shown.  The colour
convention is that positively charged tracks from V0s are red,
negatively charged tracks are green (and ``bachelors'', in the case of
cascades, purple).

By clicking on each track the values of the momentum components and
the particle mass appear in the relevant row in the bottom tool. As
you click more particles of the same decay, the tool will
automatically calculate the invariant mass for you.  Once you've
selected all daughter particles, you can make a choice of specie
($\mathrm{K}^{\mathrm{S}}_{0}$, $\Lambda$, $\bar{\Lambda}$, or $\Xi$)
in the \emph{Select particle type} selector.  Once you're happy with
your choice, press \textsl{Submit} to register the observation.

Once you have found all strange-particle decays in an event, press the
\textsl{Event analysed!} button on the left.

As you submit more and more observations of particles, we accumulate
statistics of the invariant mass spectrum for the four particles
($\mathrm{K}^{\mathrm{S}}_{0}$, $\Lambda$, $\bar{\Lambda}$, and $\Xi$)
investigated.  To see these spectra, select the \textsl{Event
  Characteristics} tab on the top.  Note, since we're using your
superb pattern recognition skills as a human (as opposed to a
computer) we do not have any ``background'' as shown in
Fig.~\ref{fig:spectra}.  Instead, all we have is the invariant mass
peaks which are centred around the known masses.

\subsection{The exercise - Analyse events and find the strange
  hadrons}

The analysis part consists of the identification and counting of
strange particles in a given event sample, typically containing 15
events. When starting the exercise, you should go to student mode and
select the event sample that you will analyse.  Currently there are 20
different event samples with data from proton proton collisions at
7TeV centre-of-mass energy.

When looking at each event display, you initially have to click on
clusters and tracks; you can observe the complexity of the events and
the high number of tracks produced by the collisions inside the
detectors.  Most of these tracks are pions.

By deselecting \textsl{Tracks} and \textsl{Clusters}, and selecting
\textsl{V0s} and \textsl{Cascades}, any tracks from V0 decays and
cascade decays will appear highlighted.  From the V0 topology you can
try to guess what the mother particle is.  By clicking on each track
you get the track information --- the charge, the three components of 
the momentum vector and the mass of the most probable particle
associated with the track. This has been found from the information
provided by the different detectors used for Particle Identification.
From the decay products you can already guess what the mother particle
is; to confirm it, you calculate the invariant mass as described in
Section~\ref{sec:invmass} and compare its value with the values on the
table of your calculator or those given in Tab.~\ref{tab:masses}

\begin{table}[htbp]
  \centering
  \begin{tabular}{|cccc|}
    \hline
    \multicolumn{2}{|c}{\textbf{Invariant mass}}
    & \textbf{Negative daughter}
    & \textbf{Parent specie}\\
    \hline
    $497\pm13$MeV
    & $[484,510]$MeV
    & -
    & $\mathrm{K}^{0}_{\mathrm{S}}$ \\
    \hline
    $1115\pm5$MeV
    & $[1110,1120]$MeV
    & $\pi^{-}$
    & $\Lambda$\\
    &
    & $\bar{\mathrm{p}}$
    & $\bar{\Lambda}$\\
    \hline
    $1321\pm10$MeV
    & $[1311,1331]$MeV
    & -
    & $\Xi^{-}$
    \\
    \hline
  \end{tabular}
  \caption{Identification criteria of strange particle decays}
  \label{tab:masses}
\end{table}

Depending on the outcome, you select the appropriate particle in the
particle selector and hit the \textsl{Submit} button. In this way this
entry is added in the corresponding invariant mass histogram. It can
happen that the calculated mass does not correspond to any of the
above values; this is ``background'': the tracks appear as coming from
a secondary vertex, but in this case the vertex has been
misidentified.  For the purpose of this exercise we will ignore these
V0's.

\subsection{Presentation of the results}

If you hit the \textsl{Print...} button on the left you will get a PDF
with the invariant mass spectra in.  If you press the
\textsl{Export...} button you can make a ROOT file with the histograms
in it.  This should be shipped to the instructor (via e-mail, network
transfer, USB-stick, or what not) so that she may collect the results
from all data sets into one final result.

\subsection{Collection of results}

Once a data set has been analysed, the corresponding ROOT file should
be shipped to the instructor.  She can then use the option
\emph{Instructor Exercise 1: Collect results} in the first interface (see
Fig.~\ref{fig:gui:ClassSelector}) which will fire up a special
interface for this purpose.  The instructor will then show the final
results to the whole class and you should discuss these in plenum. 

\section{Second exercise: Strangeness enhancement}

The event display is a powerful tool which helps check the quality of
the data and their reconstruction and gives a feeling of what events
look like.  However in real life data analysis is not done visually
--- that would be far too tedious and time-consuming.  To analyse the
millions of events that we collect daily at the LHC we run programs to
look for V0s in bigger event samples, and then build the invariant
mass spectra (as shown in Fig.~\ref{fig:spectra}) and extract the
number of strange particles, and this is what you will do here.

Once we have found the number of $\mathrm{K}^0_{\mathrm{S}}$,
$\Lambda$, and $\bar{\Lambda}$ we can calculate the yield (number of
particles produced per interaction) for each type of particle
specie.  The yield for particle specie $i$ is given by
\begin{align}
Y_i = \frac{N_i}{N_{\mathrm{events}}\varepsilon_{i}}\label{eq:yield}
\end{align}
where $N_i$ is the number of particle $i$ observed,
$N_{\mathrm{events}}$ is the number of events, and $\varepsilon_i$ is
the efficiency for detecting V0's of type $i$.  However, we want to
determine the yield for different selections of centrality.

\subsection{Centrality}

When studying heavy ion collisions we typically classify the events in
different classes, according to the centrality of the collision.

The lead nucleus is much bigger than the proton and this leads to
differences in how the particles collide. In the case of proton-proton
there is only one kind of collision while lead collisions are
differentiated by the size of the region of each nucleus that overlap.

To see the difference between proton-proton and lead-lead collisions
we first define different event classes for heavy ion collisions. One
of the criteria used is the collision centrality, which is related to
the impact parameter (the distance between the colliding nuclei
perpendicular to the beam axis). However this parameter is not
directly measurable. One of the ways in which ALICE determines the
centrality of the events is from the signal amplitude in the
V0\footnote{Not to be confused with V0's from decays.}  counters (2
arrays of plastic scintillators placed at $+330$cm and $-90$cm from
the interaction point).  This is shown in the right-hand side of
Fig.~\ref{fig:cent}.  Quantitative estimate of the collision
centrality is given by the number of participating nucleons
($N_{\mathrm{part}}$) as illustrated in red on the left-hand side of
Fig.~\ref{fig:cent}.

\begin{figure}[htbp]
  \centering
  \begin{tabular}[t]{p{.36\linewidth}p{.53\linewidth}}
    \vbox{}
    \includegraphics[width=\linewidth]{Npart}
    & \vbox{}
      \includegraphics[width=\linewidth]{Centrality}
  \end{tabular}
  \caption{Left: Illustration of number of participants
    $N_{\mathrm{part}}$ for a collision with non-zero impact parameter
    $b$. Right: Classification of events according to the signal in
    the V0 detector.  Note, small centrality classes corresponds to
    large nuclear overlap, while large centrality classes correspond
    to a small nuclear overlap.}
  \label{fig:cent}
\end{figure}

In this part of the exercise you will analyse event samples belonging
to different centrality classes looking for V0s and calculate the
number of $\mathrm{K}^0_{\mathrm{S}}$, $\Lambda$, and $\bar{\Lambda}$,
and determine the yield of each of these in those classes. 

\subsection{The exercise: Fit model to invariant mass spectra}

To start the tool, execute
\begin{quote}
  \ttfamily
  root {\itshape $\langle$installation$\rangle$}/MasterClass.C
\end{quote}
where \texttt{{\itshape $\langle$installation$\rangle$}} is where the
master class package was installed (The instructor may have installed
a Desktop launcher you can use instead).

This will bring up a window as shown on the left in
Fig.~\ref{fig:gui:ClassSelector2}.  Here, you should choose the
\emph{Strange Particle Production} from the \emph{Select class...}
selector.  Then, to start the second exercise, select
\emph{Exercise 2: Find peak over background} from the \emph{Select
  exercuse...}  selector.  This will pop up the spectra fitting tool as
shown on the right in Fig.~\ref{fig:gui:ClassSelector2}.

\begin{figure}[htbp]
  \centering
  \begin{tabular}{p{.295\linewidth}p{.67\linewidth}}
    \includegraphics[width=\linewidth]{../../Base/doc/ClassSelector}
    & \includegraphics[width=\linewidth]{SpectraFit}
  \end{tabular}
  \caption{Left: Master Class selector.  Choose \emph{Strange Particle
      Production} and \emph{Exercise
      2: Fit peak over background}.  Right: Tool to analyse invariant
    mass spectra.}
  \label{fig:gui:ClassSelector2}
\end{figure}

On the left of the spectra fitting tool you have a tree of spectra to
fit.  Click a spectra to show it on the right-hand side.  Then, try to
fit our signal-background model to the spectra by pressing
\textsl{Fit} in the bottom.  You can limit the placement of the peak
by using the slider \emph{Signal limits} on the bottom. You can also
limit the rang of the whole fit using the slider \emph{Fit range}.
After having performed a fit, the model with fitted parameters is
shown as a line on the spectra.  Also shown in the reduced $\chi^2$
($\chi^2/\nu$).  This number should be close to unity (1).

Once you're happy with the fit, you can press the \textsl{Accept}
button and move on to the next spectra.  Pressing the \textsl{Accept}
button marks the spectra as done, stores the fit, and the extracted
number of strange-particles as described in Section~\ref{sec:sigbck}.
You can see these by selecting the tab \textsl{Summary} on the top.
Also stored is the extracted invariant mass and width of the signal
(with errors).  These you can see in the tab \textsl{Mass \& Width}.

Note, you will not see the actual number of strange particles found
for a given spectrum until you hit the \textsl{Accept} button.  This
is because the $\chi^2/\nu$ is all you need to determine if the fit is
good or not.  Once you accept the fit, you will get the extracted
values.  This is akin to what we all \emph{Blind Analysis}. 

You should do as many fits as possible.  The instructor may give you a
set of priorities to complete first. Once you're done with the fits
you need to do, you should press the \textsl{Export...} button to save
the results in to a ROOT file. 

After we've determined the number of $\mathrm{K}^0_{\mathrm{S}}$,
$\Lambda$, and $\bar{\Lambda}$ in each centrality range, we can
calculate the yield in each centrality range.  For this we need a few
numbers given in Tab.~\ref{tab:numbers}.  Then finally, we can
calculate the ratio of strange particles in heavy-ion collisions to
the same in proton-proton collisions as
\begin{align}
  \label{eq:enhance}
  H_i &=
        \frac{Y^{\mathrm{Pb-Pb}}_i}{N^{\mathrm{Pb-Pb}}_{\mathrm{part}}}
        \frac{N^{\mathrm{pp}}}{Y^{\mathrm{pp}}_i}\quad,
\end{align}
with $Y_i$ calculated according to \eqref{eq:yield} for each
centrality bin heavy-ion collisions and once all proton-proton bins.

If this number is larger than 1, then we have \emph{strangeness
  enhancement} --- that is, it is easier to make strange quarks in the
hot and dense medium created in heavy-ion collisions than in
proton-proton collisions.

\begin{table}[htbp]
  \centering
  \begin{tabular}{|c|rr|rrr|}
    \hline
    \textbf{Centrality}
    & $\mathbf{N}_{\mathbf{part}}$
    & $\mathbf{N}_{\mathbf{events}}$
    & $\varepsilon_{\mathrm{K}^0_{\mathrm{S}}}$
    & $\varepsilon_{\Lambda}$
    & $\varepsilon_{\bar{\Lambda}}$
    \\[1ex]
    \hline
    pp
    & $2$
    & $5260$
    & $0.26$
    & $0.67$
    & $0.44$
    \\
    $\phantom{8}0-10$
    & $360$
    & $213$
    & $0.26$
    & $0.20$
    & $0.20$
    \\
    $10-20$
    & $260$
    & $290$
    & $0.26$
    & $0.21$
    & $0.21$
    \\
    $20-30$
    & $186$
    & $302$
    & $0.29$
    & $0.22$
    & $0.22$
    \\
    $30-40$
    & $129$
    & $310$
    & $0.29$
    & $0.22$
    & $0.22$
    \\
    $40-50$
    & $85$
    & $302$
    & $0.29$
    & $0.22$
    & $0.22$
    \\
    $50-60$
    & $52$
    & $300$
    & $0.29$
    & $0.20$
    & $0.20$
    \\ 
    $60-70$
    & $30$
    & $315$
    & $0.35$
    & $0.20$
    & $0.20$
    \\ 
    $70-80$
    & $16$
    & $350$
    & $0.26$
    & $0.20$
    & $0.20$
    \\
    \hline
  \end{tabular}
  \caption{Auxiliary numbers}
  \label{tab:numbers}
\end{table}
\end{document}

%  LocalWords:  hyperon topologies colour
