ALICE Master Classes {#index}
=============================

This package contains the two available Master Classes from ALICE. 

Currently, two master classes are available 

- Nuclear Modification Factor 

  In this master class the students will measure the nuclear
  modification factor, which expresses the change in particle
  production in heavy-ion collisions as compared to proton-proton
  collisions. 
  
- Strangeness Enhancement 

  In this master class the students will measure the strangeness
  enhancement in heavy-ion collisions as compare to proton-proton
  collisions. 
  
Longer descriptions of the master classes
-----------------------------------------

Description of the individual classes 

- [RAA class][raadoc]
- [Strangeness class][strdoc]

The same descriptions can be found in the file `Documentation.pdf` in
`doc` sub-directory of the class.

- `Raa/doc/Documentation.pdf` Description (including physics
  motivation) of the Nuclear Modification master class 
- `Strangeness/doc/Documentation.pdf` Description (including physics
  motivation) of the Strangeness Enhancement master class
  
The students should read the descriptions. 
  

Modes of running the Master Classes
-----------------------------------

You have three options for how to run these master classes: 

- Execute via [Docker][docker] using a modern Web-browser to display the 
  classes.  This will run on GNU/Linux, MacOSX, and Windows.  This requires 
  - An installation of Docker 
  - A modern web-browser (recent versions of Chrome, Firefox, Safari, and 
    Edge should all do)
- Execute via [Docker][docker] using an X window system server.  This can in 
  principle run on GNU/Linux, MacOSX, as well as Windows.  This requires 
  - An installation of the X window system that supports hardware acceleration 
    for OpenGL rendering of the [Docker][docker] application.  Some graphics 
    vendors (most notably nVidia) does not provide support for this in their 
    HW/driver stack.  If that is the case, please use one of the other two 
   options. 
  - An installation of [Docker][docker] 
- Native installation: On GNU/Linux or MacOSX, download one of the distribution
  packages below and unpack to some directory.  This requires
  - An installation of the X window system that supports hardware acceleration 
    for OpenGL rendering.  On GNU/Linux this is usually not a problem.  However, 
    if you have trouble with hardware acceleration, choose the first method above. 
  - An installation of [ROOT][root] (versions 5.34 and 6+ are both supported)

Of these, the first is by far the least invasive and requires the least set-up. 

Installation
------------

For [Docker][docker] deployment, see  [these instructions](.docker/README.md), 
and then skip [down](#class-selection).

For native installations on GNU/Linux and MacOSX, keep reading.

The instructor/teacher/student can download one of the below
distributions and unpack them on their machine. 

### Requirements

- The host you wish to install on must run GNU/Linux or MacOSX
- Installation of [ROOT][root]
  - For MacOSX grab a binary blob from
    [the ROOT Download page][rootdown].  Note you must also make sure that 
    you have [XQuartz][xquartz] installed, and that it supports hardware acceleration.
  - For Linux, depending on your distribution, you may be able to find
    packages in the distribution package repository - search for names
    like `root`, `root-system`, or `cernroot`.  Otherwise, you can get
    a binary blob from [the ROOT Download page][rootdown].  Please make 
    sure that your graphics stack support hardware acceleration (you can test
    this by executing `glxgears` or `glxinfo`)
	
### Distribution downloads 

Currently there are three packages available 

- *ALICE_Raa* which contains the nuclear modification factor master
  class. 
- *ALICE_Strangeness* which contains the strangeness enhancement
  master class 
- *ALICE_MasterClasses* which contains all of the above. 

Further more you can get two version of each of these 

- *Latest* which contains the most recent changes to the classes, but
  also potential bugs.
- *Versioned* which are fixed (and presumably) stable classes. 

#### Latest:

- [ALICE_MasterClasses][full-master]
- [ALICE_Raa][raa-master]
- [ALICE_Strangeness][str-master]

#### Versioned - 0.2

- [ALICE_MasterClasses][full-0.2]
- [ALICE_Raa][raa-0.2]
- [ALICE_Strangeness][str-0.2]

### Sources 

The project is managed at the
[CERN Gitlab instance](https://gitlab.cern.ch/cholm/alice-masterclasses)

### Unpacking and setting up

The packages are distributed as ZIP archives.  Unpack the distribution
you got to somewhere that makes sense in your installation
environment - say `/opt/` or your home directory. For example

	unzip ALICE_MasterClasses-0.2.tar -d /opt
	
You may want to copy the the file `AliceMasterClass.desktop` to the
`Desktop` directory of the users that will run the exercises so they
can launch the class directly from the desktop.  You should, however,
edit that file to give the path to the directory where you unpacked
the package, and make sure that the `root` executable can be found.
For example, if you took the *ALICE_Raa* master class unpacked in
`/opt`, and your ROOT installation is in `/opt/root` then the
`AliceMasterClass.desktop` file should read

	[Desktop Entry]
	Name=ALICE Master classes
	GenericName=ALICE Master classes
	Comment=ALICE master classes
	Exec=/opt/root/bin/root -l /opt/ALICE_Raa/MasterClass.C
	Icon=/opt/ALICE_Raa/Base/doc/ALICE_Logo.png
	Terminal=true
	Type=Application
	MimeType=application/x-root;
	Categories=Science;
	Encoding=UTF-8
	X-Desktop-File-Install-Version=0.21

It is recommended that you take the *ALICE_MasterClasses* package so
that you may use the same installation in all cases. 

Usage
-----

Just run the script `MasterClass.C` and the graphical user interfaces
will guide you through the chosen Master Class.  That is, do 

	root _path_/MasterClass.C 
	
where _path_ is where you installed this package. 

### Class selection

This will bring up a GUI where you can select your Master Class and
the specific exercise.

![Start GUI](Base/doc/ClassSelector.png)

*Note*: The above image does not show recent additions to this
interface. 

- At the top, you can select the source location of data files. If you
  do not choose one, you will use the default locations.  Use this to
  enter a custom location where the data files can be found.
- You can also choose the language used in the descriptions through
  out the master classes.  Note, that this does not effect the
  _interface_ language which will remain English.  Note also, that
  some descriptions may not be available in the chosen language, and
  your favorite language may not be an option.  In that case, please
  contribute to this project by translating the various HTML files in
  this project and submit them through the Issues link at
  gitlab.cern.ch. 

Distribution
------------

As mentioned above, there are three distributions.  To make these do 

	make raadist 
	make strdist
	make dist
	
to make the *ALICE_Raa*, *ALICE_Strangeness*, and full
*ALICE_MasterClasses* distributions, respectively.

Optionally, one can make distributions without all the data in.  In
this case, the data will be read via HTTP from
http://cern.ch/cholm/alice-masterclass.  To make these tar-balls, do 

	make raadist_nodata
	make strdist_nodata
	make dist_nodata
	
More information
----------------

The code is pretty thoroughly commented and documented.  You can find
the code documentation on
[a separate page](https://cern.ch/cholm/alice-masterclasses). Some
thoughts on the implementation can be found in
[a separate file](Base/doc/Implementation.md)

[root]: https://root.cern.ch
[rootdown]: https://root.cern.ch/downloading-root
[docker]: https://www.docker.com/
[xquartz]: https://www.xquartz.org/
[xming]: https://sourceforge.net/projects/xming/
[roothub]: https://hub.docker.com/r/rootproject/root-ubuntu16/
[full-master]: https://gitlab.cern.ch/cholm/alice-masterclasses/builds/artifacts/master/download?job=fulldist
[raa-master]: https://gitlab.cern.ch/cholm/alice-masterclasses/builds/artifacts/master/download?job=raadist
[str-master]: https://gitlab.cern.ch/cholm/alice-masterclasses/builds/artifacts/master/download?job=strdist
[full-0.2]: https://gitlab.cern.ch/cholm/alice-masterclasses/builds/artifacts/v0.2/download?job=fulldist
[raa-0.2]: https://gitlab.cern.ch/cholm/alice-masterclasses/builds/artifacts/v0.2/download?job=raadist
[str-0.2]: https://gitlab.cern.ch/cholm/alice-masterclasses/builds/artifacts/v0.2/download?job=strdist
[raadoc]: https://gitlab.cern.ch/cholm/alice-masterclasses/builds/artifacts/master/file/ALICE_MasterClass-master/Raa/doc/Documentation.pdf?job=fulldist
[strdoc]: https://gitlab.cern.ch/cholm/alice-masterclasses/builds/artifacts/master/file/ALICE_MasterClass-master/Strangeness/doc/Documentation.pdf?job=fulldist
EOF

